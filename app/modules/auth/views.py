from flask import Flask, g, render_template, request, redirect, url_for, current_app as app
from flask import Blueprint
from flask_login import login_user, login_required, current_user, logout_user

from app.extensions.auth import http_token_auth

from app.modules.users.models import User
from . import jwt_validator

AWS_REGION='us-west-2'

def get_claims(id_token):
    return jwt_validator.get_claims(AWS_REGION,
                                    app.config['AWS_COGNITO_USER_POOL'],
                                    id_token,
                                    app.config['AWS_COGNITO_CLIENT'])


auth_blueprint = Blueprint('auth', __name__, url_prefix='/auth')

@auth_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    return 'inlogin'


@auth_blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    return 'inlogout'


@auth_blueprint.route('/cognito_signin')
def cognito_signin():
    ''' Token is returned behind # sign, which means the server does not see it.
    Return the same page. Let the javascript library do its thing. Then
    Redirect back to flask with the token as a parameter or a cookie, so we can see it here. '''
    # id_token = request.args.get('id_token')
    # id_claims = get_claims(id_token)
    # import pprint
    # id_claims_str = pprint.pformat(id_claims)
    return render_template('testcognito2.html',config=app.config)


@auth_blueprint.route('/cognito_signin2')
def cognito_signin2():
    id_token = request.args.get('id_token')
    id_claims = get_claims(id_token)
    access_token = request.args.get('access_token')
    access_claims = get_claims(access_token)
    import pprint
    print('ID_TOKEN:'+id_token)
    pprint.pprint(id_claims)
    print('ACCESS_TOKEN:'+access_token)
    pprint.pprint(access_claims)
    user = User()
    user.email = id_claims.get('email')
    login_user(user)
    return render_template('signin.html')
 

@auth_blueprint.route('/cognito_signin3')
@http_token_auth.login_required
def cognito_signin3():
    print(f'success user is {g.current_user.email}')
    return f'success user is {g.current_user.email}'


@auth_blueprint.route('/cognito_signout')
def cognito_signout():
    return render_template('testcognito2.html',config=app.config)


@auth_blueprint.route('/test_token1')
@http_token_auth.login_required
def test_token1():
    import pprint
    html_str = ''
    if g.current_user.id_token:
        html_str += f'id_token:<textarea rows=5 cols=100>{g.current_user.id_token}</textarea><br/>\n'
        html_str += f'id_claims:<textarea rows=5 cols=100>{g.current_user.id_claims}</textarea><br/>\n'
        html_str += f'access_claims:<textarea rows=5 cols=100>{g.current_user.access_claims}</textarea><br/>\n'
    if request.cookies:
        print('id_token:'+str(request.cookies.get('id_token')))
        print('access_token:'+str(request.cookies.get('access_token')))
    if request.headers:
        print('CognitoIdentityId:'+str(request.headers.get('CognitoIdentityId')))
        print('user_id:'+str(request.headers.get('user_id')))
        pprint.pprint(request.headers)
    return html_str


@http_token_auth.verify_token
def verify_token(tokens):
    ''' @http_token_auth.login_required decorated methods below will call verify_token first.
    sets g.current_user 
    '''
    auth_user = User.from_auth_tokens(tokens)
    if isinstance(auth_user, str):
        return False
    else:
        g.current_user = auth_user
        return True
