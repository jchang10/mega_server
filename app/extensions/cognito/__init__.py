
from .cognito import Cognito
cognito = Cognito()

def init_app(app):
    cognito.init_app(app)
    
