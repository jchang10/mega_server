
from flask_restplus_patched import Api

authorizations = {
    'token':{
        'type':'apiKey',
        'in':'header',
        'name':'Authorization'
    },
}

api_v1 = Api(
    version='1.0',
    title='Withme API',
    description='Withme API Description TBD',
    authorizations=authorizations,
)

def init_app(app):
    pass
