import types
from app.extensions.logging import logging
log = logging.getLogger(__name__)
from functools import wraps
from botocore.exceptions import ClientError
from flask import Flask, g, render_template, request, redirect, url_for, current_app, make_response, jsonify
from werkzeug.exceptions import HTTPException
from flask_restplus_patched import Namespace, Resource
from flask_restplus_patched._http import HTTPStatus

from app.modules.users.models import User
from . import schemas

api = Namespace('auth', description='Users description')

def authenticate(admin=False):
    """Authorization header token must be valid."""
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            response_object = {
                'status': 'error',
                'message': 'Something went wrong. Please contact us.'
            }
            code = 401

            auth_header = request.headers.get('Authorization')
            bypass_hack = request.headers.get('Bypass')

            if auth_header and not bypass_hack:
                if auth_header.startswith('Bearer '):
                    bearer, auth_token = auth_header.split(None, 1)
                    log.info('Token given with Bearer')
                else:
                    auth_token = auth_header
                    log.info('Token given without Bearer')

                log.info('Setting AUTH_USER')
                auth_user = User.from_auth_tokens(auth_token)
                if isinstance(auth_user, str):
                    response_object['message'] = auth_user
                    return make_response(jsonify(response_object), code)
                log.info('AUTH_USER is now set')

            else:
                if bypass_hack or current_app.config.get('DEBUG', False):
                    # in DEBUG mode, allow unauthorized user
                    #JAE HACK bypass_hack to workaround an unknown flask_graphql, i think, bug
                    auth_user = _unauthorized_user()
                else:
                    log.warn('in PRODUCTION mode, authorization TOKEN missing.')
                    response_object['message'] = 'Provide a valid auth token.'
                    code = 403
                    return make_response(jsonify(response_object), code)

            log.info('AUTH_USER is now set')

            #admin check
            log.info('Checking cognito:groups')
            groups = getattr(auth_user,'cognito:groups',[])
            if not groups:
                groups = []
            if current_app.config.get('ADMINS_GROUP') in groups:
                auth_user.is_admin = True
            else:
                auth_user.is_admin = False

            # admin check
            if admin and not auth_user.is_admin:
                log.warn('PERMISSIONS: Admin rights are required')
                response_object['message'] = 'Admin rights are required.'
                return make_response(jsonify(response_object), 403)
            
            #set globally
            g.current_user = auth_user

            return func(*args, **kwargs)
        return wrapper
    return decorator


def _unauthorized_user():
    return types.SimpleNamespace(
        unauthorized = True,
        username = "unauthorized",
        is_admin = False,
        sub = None,
        email = None
    )

def _current_user():
    if g.get('current_user'):
        return g.current_user

    #unauth user
    g.current_user = _unauthorized_user()
    return g.current_user

from werkzeug.local import LocalProxy
current_user = LocalProxy(lambda: _current_user())


from flask_marshmallow import base_fields
import flask_restplus_patched
class LoginParameters(flask_restplus_patched.Parameters):
    username = base_fields.String(description="Username", required=True)
    password = base_fields.String(description="No rules yet", required=True)


@api.route('/login')
class LoginUser(Resource):
#    def post(self):
    @api.parameters(LoginParameters())
    @api.response(schemas.LoginResponseSchema())
    def post(self, args):
        """
        User Login
        Returns: AUTH_TOKEN to be used with subsequent protected API calls.
        """
        # get post data
        username = args.get('username')
        password = args.get('password')
        try:
            # fetch the user data
            try:
                user = User.get_user(username=username)
            except ClientError as e:
                api.abort(HTTPStatus.NOT_FOUND,
                          status='error',
                          message='User does not exist.')

            try:
                warrant = User.warrant_authenticate(username,password)
            except Exception as e:
                api.abort(HTTPStatus.NOT_FOUND,
                          status='error',
                          message=str(e))
            response_object = dict(
                status= 'success',
                message= 'Successfully logged in.',
                auth_token = f'{warrant.id_token}:{warrant.access_token}' if warrant.id_token \
                  and warrant.access_token else 'Invalid'
            )
            return response_object
        
        except HTTPException as e:
            raise
        except Exception as e:
            print(e)
            api.abort(500, 'Try again.',
                      status='error')


@api.route('/logout')
class LogoutUser(Resource):
    @authenticate()
    @api.doc(security='token')
    @api.response(schemas.BasicAuthResponseSchema())
    def get(self):
        """
        User Logout
        """
        response_object = dict(
            status='success',
            message='Successfully logged out.',
        )
        return response_object, 200


@api.route('/status')
class GetUserStatus(Resource):
    @authenticate()
    @api.response(schemas.StatusResponseSchema())
    def get(self):
        """
        User Login Status
        Returns the AUTH_TOKEN claims
        """
        #user = User.query.filter_by(id=resp).first()
        user = g.current_user
        response_object = dict(
            status= 'success',
            user= dict(
                id = getattr(user, 'sub', 'None'),
                username = getattr(user, 'username', 'None'),
                email = getattr(user, 'email', 'None'),
                enabled = getattr(user, 'enabled', 'None'),
                create_date = getattr(user, 'create_date', 'None'),
                is_admin = getattr(user, 'is_admin', 'None'),
            )
        )
        return response_object, 200

"""
@token_auth.verify_token
def verify_token(auth_token):
    ''' @token_auth.login_required decorated methods below will call verify_token first.
    sets g.current_user 
    '''
    import ipdb; ipdb.set_trace()
    
    payload = User.decode_auth_token(auth_token)
    user = User(payload['email'], payload['name'], payload['sub'])
    user.id_token = auth_token
    user.payload = payload
    g.current_user = user
    return True

    try:
        print('VERIFY_TOKEN:',token)
        id_token = token
        id_claims = get_claims(id_token)
        print('VERIFY_TOKEN CLAIMS:')
        import pprint
        pprint.pprint(id_claims)
        user = User()
        user.email = id_claims.get('email')
        user.id_token = token
        user.claims = id_claims
        g.current_user = user
        return True
    except:
        pass
    return False
"""
