import datetime
import os
import time

from flask_testing import TestCase

from app import create_app
from app.modules.users.models import User


class BaseTestCase(TestCase):


    def _add_test_user1(self):
        self.test_user1 = self.create_test_user('jc-test1',
                                                'Test User',
                                                'Test2test',
                                                email='jc-test1@jline.com',
                                                phone_number='+1231231234')
        return self.test_user1

    def _add_test_user2(self):
        self.test_user2 =  self.create_test_user('jc-test2',
                                                 'Test User',
                                                 'Test2test',
                                                 email='jc-test2@jline.com',
                                                 phone_number='+1231231234')
        return self.test_user2

    def create_test_user(self, username, name, password, email=None, is_add_to_test_group=True, **kwargs):
        """
        Create a test user.
        is_test_group - bool - adds to TestGroup by default.
        """
        warrant = User.warrant(username)
        if not email:
            raise ValueError('Email is required when creating user')
        warrant.admin_create_user(username, temporary_password=password, name=name, email=email, **kwargs)
        user = warrant.admin_get_user()
        warrant.new_password_challenge(password, password)

        if is_add_to_test_group:
            self.add_user_to_test_group(user)
            
        return user


    def add_user_to_test_group(self, user):
        # _metadata['username'] is required here to prevent "User does not exist errors." this is
        #  the cognito:username which can be username, email or sub.
        cognito_username = user._metadata['username']
        User.cognito.admin_add_user_to_group(cognito_username, self.UNITTEST_GROUP)
            

    def create_admin_test_user(self, username, name, password, **kwargs):
        user = self.create_test_user(username, name, password, **kwargs)
        cognito_username = user._metadata['username']
        User.cognito.admin_add_user_to_group(cognito_username, self.ADMINS_GROUP)
        return user


    def create_test_group(self):
        if getattr(self, 'test_group', None):
            return
        try:
            User.cognito.create_group(self.UNITTEST_GROUP)
        except Exception as e:
            pass
        warrant = User.warrant()
        self.test_group = warrant.get_group(self.UNITTEST_GROUP)


    def delete_all_test_users(self):
        """Remove all test users"""
        if getattr(self, 'test_user1', None):
            try:
                User.cognito.admin_delete_user(self.test_user1.username)
            except Exception:
                pass
            del self.test_user1
        if getattr(self, 'test_user2', None):
            try:
                User.cognito.admin_delete_user(self.test_user2.username)
            except Exception:
                pass
            del self.test_user2
        
        response = User.cognito.list_users_in_group(self.UNITTEST_GROUP)
        for user in response['Users']:
            print(f'Deleting user username="{user["Username"]}" {user["Attributes"]}')
            User.cognito.admin_delete_user(user['Username'])

    def create_app(self):
        # make sure AWS_PROFILE is set
        # if os.environ.get('STAGE','default') == 'default':
        #     self.assertTrue(os.environ.get('AWS_PROFILE'))
        self.assertTrue(os.environ.get('AWS_PROFILE'))
        self.app = create_app()
        self.UNITTEST_GROUP = self.app.config.get('UNITTEST_GROUP')
        self.ADMINS_GROUP = self.app.config.get('ADMINS_GROUP')
        self.assertTrue(self.UNITTEST_GROUP)
        self.assertTrue(self.ADMINS_GROUP) 
        return self.app


    @classmethod
    def setUpClass(cls):
        pass


    def setUp(self):
        self.create_test_group()
        self.delete_all_test_users()


    def tearDown(self):
        self.delete_all_test_users()
