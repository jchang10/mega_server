import flask_marshmallow
from flask_marshmallow import base_fields

from . import models

class GroupSchema(flask_marshmallow.Schema):
    name = base_fields.Str(attribute='GroupName')
    description = base_fields.Str(attribute='Description')
        

class UserSchema(flask_marshmallow.Schema):
    id = base_fields.Str()
    email = base_fields.Str()
    username = base_fields.Str()
    create_date = base_fields.DateTime()
    enabled = base_fields.Bool()
    groups = base_fields.Nested(GroupSchema, attribute="groups_response", many=True)





