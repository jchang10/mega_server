from deprecated import deprecated

import boto3
import botocore
from botocore.exceptions import ClientError

from warrant import Cognito as WarrantCognito

class Cognito():

    def __init__(self, userpool_id=None, client_id=None):
        self.cognito_idp = boto3.client('cognito-idp', region_name="us-west-2")
        self.userpool_id = userpool_id
        self.client_id = client_id
        

    def init_app(self, app):
        """ Flask initializer """
        self.userpool_id = app.config.setdefault('AWS_COGNITO_USER_POOL', None)
        self.client_id = app.config.setdefault('AWS_COGNITO_CLIENT', None)


    ''' COGNITO '''

    def admin_get_user(self, username):
        response = self.cognito_idp.admin_get_user(**{
            'UserPoolId':self.userpool_id,
            'Username':username
        })
        return response


    # use warrant.admin_create_user() instead
    @deprecated
    def admin_create_user(self, username, name, temp_password=None):
        response = None
        try:
            args = {
                'UserPoolId':self.userpool_id,
                'Username' : username,
                'UserAttributes' : [
                    {'Name':'name',
                     'Value':name},
                    {'Name':'email',
                     'Value':username},
                ],
                'MessageAction' : 'SUPPRESS'
            }
            if temp_password:
                args['TemporaryPassword'] = temp_password
            response = self.cognito_idp.admin_create_user(**args)
        except botocore.exceptions.ClientError as ex:
            raise ValueError(ex.response['Error']['Code'])

        return response

    
    def admin_add_user_to_group(self, sub, groupname):
        """Add user to group. Must provide sub for user"""
        self.cognito_idp.admin_add_user_to_group(**{
            'UserPoolId':self.userpool_id,
            'Username':sub,
            'GroupName':groupname
        })


    def list_users_by_email(self, email):
        response = self.cognito_idp.list_users(**{
            'UserPoolId':self.userpool_id,
            'Filter':f'email = "{email}"'
        })
        return response


    def admin_list_groups_for_user(self, username):
        response = self.cognito_idp.admin_list_groups_for_user(
            Username=username,
            UserPoolId=self.userpool_id
        )
        return response
    

    def list_users_in_group(self, group_name):
        response = self.cognito_idp.list_users_in_group(
            UserPoolId=self.userpool_id,
            GroupName=group_name
        )
        return response


    def admin_delete_user(self, username):
        response = self.cognito_idp.admin_delete_user(
            UserPoolId=self.userpool_id,
            Username=username
        )
        return response


    def create_group(self, groupname):
        response = self.cognito_idp.create_group(
            UserPoolId=self.userpool_id,
            GroupName=groupname,
            Description='Auto-created for unit testing'
        )
        return response
    
