
from .models import NotesDb


import flask_marshmallow    
import marshmallow_pynamodb
class NotesDbSchema(flask_marshmallow.Schema, marshmallow_pynamodb.ModelSchema):
    class Meta:
        model = NotesDb


import flask_restplus_patched
import flask_marshmallow
from flask_marshmallow import base_fields
class AddNotesParameters(flask_restplus_patched.Parameters):
    note1 = base_fields.Str()
    note2 = base_fields.Str()
    note3 = base_fields.Str()
