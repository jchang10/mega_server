from flask import Flask, render_template, g, request

import config
from config.config import config_map
from config.secrets import secrets_map

def create_app(config_name=None):
    """Create an application instance."""
    app = Flask(__name__)
    app.jinja_env.auto_reload = True

    # apply configuration
    if config_name is None:
        config_name = config.config.get_stage()
    app.config.from_object(config_map[config_name])
    app.config.from_object(secrets_map[config_name])

    # initialize extensions
    from . import extensions
    extensions.init_app(app)

    # initialize modules
    from . import modules
    modules.init_app(app)

    # register blueprints
    # from .auth import auth as auth_blueprint
    # app.register_blueprint(auth_blueprint, url_prefix='/auth')

    # from api.auth_view import auth_view
    # app.register_blueprint(auth_view,  url_prefix='/api')

    # from api.cognito_view import cognito_view
    # app.register_blueprint(cognito_view,  url_prefix='/api')

    # api version 1
    # from api.users_view import user_view
    # app.register_blueprint(user_view,  url_prefix='/api')
    # from api.users_view import users_view
    # app.register_blueprint(users_view,  url_prefix='/api')
    # api version 2
    # from api import api2_view
    # app.register_blueprint(api2_view, url_prefix='/api')

    # from app.users_view import user_view
    # app.register_blueprint(user_view, url_prefix='/app')
    # from api.users_view import users_view
    # app.register_blueprint(users_view, url_prefix='/app')

    @app.route('/')
    def index():
        return render_template('index.html')

    @app.route('/testcognito')
    def testcognito():
        return render_template('testcognito.html')

    @app.route('/testcognito2')
    def testcognito2():
        return render_template('testcognito2.html')

    @app.route('/test_token2')
    def test_token2():
        import pprint
        html_str = ''
        if request.cookies:
            print('id_token:'+str(request.cookies.get('id_token')))
            print('access_token:'+str(request.cookies.get('access_token')))
        if request.headers:
            print('CognitoIdentityId:'+str(request.headers.get('CognitoIdentityId')))
            print('user_id:'+str(request.headers.get('user_id')))
            pprint.pprint(request.headers)
        return html_str

    @app.route('/test_react')
    def test_react():
        return render_template('test_react.html')

    from app.modules.auth.resources import authenticate

    @app.errorhandler(400)
    def bad_request(error):
        app.logger.debug('HERE')
        return 'bad request', 400
        
    @app.errorhandler(401)
    def bad_request2(error):
        app.logger.debug('HERE')

    @app.route('/ping')
    @authenticate()
    def ping():
        # import ipdb;ipdb.set_trace()
        from flask import abort, Response, current_app
        return Response(status=400, response='test mesg')
        #return 'error msg', 400
        # abort(400, 'abort msg')

    @app.route('/testing')
    def testing():
        return render_template('testing.html')

    return app


def push_context():
    app = create_app()
    app.app_context().push()
