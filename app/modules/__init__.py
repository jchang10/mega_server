
def init_app(app):
    from . import api
    from . import appsync
    from . import auth
    from . import cognito
    from . import faces
    from . import faces_v2
    from . import graphql
    from . import s3faces
    from . import users
    for module in (
            appsync,
            auth,
            cognito,
            graphql,
            notes,
            users,
            api #API Must be last!!!
            ):
        module.init_app(app)
        
