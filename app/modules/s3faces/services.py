from flask import g
from typing import List, Tuple, Dict, Any, Iterator
import boto3, datetime, logging, os, re, types, uuid
from app.modules.faces.models import FaceAssetsDb, FacePartsDb, FaceMarksDb
from app.modules.faces.s3 import BUCKET_NAME, BUCKET_PREFIX, get_bucket

log = logging.getLogger(__name__)


_PARTS_FILTER = ['hair/','_baseHead_']
_PARTS = ['hair','basehead']
#_EXTS_INCLUDE = ['.fbx','.png']
_EXTS_INCLUDE:List[str] = ['']
_EXTS_EXCLUDE:List[str] = []

def grep_part(path:str) -> str:
    """ determine if any part names are found """
    for index, part in enumerate(_PARTS_FILTER):
        if path.find(part) > -1:
            return _PARTS[index]
    return None

def grep_gender(path:str) -> str:
    if path.find('female') > -1:
        return 'female'
    elif path.find('male') > -1:
        return 'male'
    else:
        return None

def grep_prefix_key(key:str, prefix:str) -> str:
    if prefix.endswith('/'):
        return key[len(prefix):] # remove prefix
    else:
        return key[len(prefix)+1:] # remove prefix

def grep_face_path(path:str):
    """ return the bucket/prefix/facename path only. strip the part path. """
    face_path = re.sub(f'(^{BUCKET_NAME}/{BUCKET_PREFIX}\/[^\/]+)/(.*)', '\g<1>', path)
    return face_path

def filter_exts(part_path:str) -> bool:
    if not any(part_path.lower().endswith(s) for s in _EXTS_INCLUDE):
        log.info('Not in _EXTS_INCLUDE "part_path". skipping...')
        return True

    if any(part_path.lower().endswith(s) for s in _EXTS_EXCLUDE):
        log.info('Exists _EXTS_EXCLUDE in "part_path". skipping...')
        return True
    
    return False

def process_face_asset(
        bucket_name:str, 
        prefix:str, 
        objects_list = None
    ) -> FaceAssetsDb:
    """ Process S3 files. Start at the given path."""
    if not prefix.endswith('/'):
        prefix += '/'
    # re-arrange vars here
    full_face_path = f'{bucket_name}/{prefix}'
    face_path = grep_face_path(full_face_path)
    face_name = os.path.basename(face_path[:-1])
    # face_prefix = os.path.dirname(face_path[:-1]) + '/'

    """ see if a FaceAsset match is found first. if so, use that Face Asset, 
        instead of creating a new one. """
    face = None
    for aface in FaceAssetsDb.scan():
        """ TODO replace scan() call with something better. """
        if aface.path in face_path:
            face = aface
            break

    if not face:
        """ create a new face then """
        face = FaceAssetsDb.get_or_create(face_path)
        face.name = face_name
        face.path = face_path
        try:
            face.save(condition = (FaceAssetsDb.name != face.name) | (FaceAssetsDb.path != face.path))
        except:
            log.debug('Dynamodb face condition no change')

    parts_map = { p.path:p for p in face.parts }
    s3objects = fetch_s3objects(bucket_name, prefix, objects_list)
    parts = []
    for key, s3obj in s3objects.items():
        log.debug(f'key:{s3obj.key} size:{s3obj.size}')
        part_path = grep_prefix_key(s3obj.key, face.prefix)

        if not face.prefix in s3obj.key:
            """ ensure the part matches the face """
            break

        if filter_exts(part_path):
            # include and exclude filters
            continue
        
        if s3obj.key.endswith('/'):
            # skip directory paths
            continue

        # try assigning some fields
        owner = g.current_user.username if g.current_user else None
        size = s3obj.size
        part_name = grep_part(part_path)
        gender = grep_gender(part_path)
        etag = s3obj.e_tag
        part_key = os.path.join(face.name, part_path)
        
        part = parts_map.get(part_path)
        if not part:
            part = FacePartsDb.get_or_create(face, part_path)
        
        # workaround: dynamodb conditional expressions cannot handle complex expressions with more than 2 conditions.
        if any([part.etag != etag,
                part.part != part_name,
                part.gender != gender]):
            do_save = True
        else:
            do_save = False

        part.set_attributes(**dict(
            path = part_path,
            size = size,
            gender = gender,
            part = part_name,
            owner = owner if owner != 'unauthorized' else None,
            etag=etag,
            partKey=part_key,
            # owner = 'owner_tbd',
            # #region # handle versioning
            # version_id = latest_version_id
            # #endregion
        ))
        try:
            # cant get a more complicated condition to work
            # part.save(condition = (FacePartsDb.etag != part.etag) | ((FacePartsDb.part != part.part) | (FacePartsDb.gender != part.gender)))
            if do_save:
                part.save()
                log.debug('saved... '+part_path)
        except Exception as ex:
            log.debug('Dynamodb part condition no change: '+str(ex))
        parts.append(part)

    # identify face.parts that no longer exist in S3. these files are
    # assumed to be deleted, then. mark them as deleted in dynamodb, too.
    deleted_parts = [ part for part in face.parts
                        if not part in parts and prefix in part.s3_key]
    for part in deleted_parts:
        if not part.deleted:
            part.deleted = True
            part.save()
            log.debug(f'Marked deleted... part.path:{part.path}')

    face.parts = parts
    return face


def process_face_container(bucket_name:str, prefix:str) -> List[FaceAssetsDb]:
    """ process a container of face assets. return a list of faces that were processed. """
    bucket = get_bucket(bucket_name)
    s3objects:Dict[str, List] = {}
    for obj in bucket.objects.filter(Prefix=prefix):
        if obj.key.endswith('/'): continue
        key = obj.key
        key = grep_prefix_key(key, prefix)
        (face_name, part_path) = key.split('/',1)
        s3objects.setdefault(face_name, []).append(obj)

    faces_result = []
    for face_name in s3objects:
        face_prefix = os.path.join(prefix, face_name)
        face = process_face_asset(bucket_name, face_prefix, objects_list=s3objects[face_name])
        faces_result.append(face)

    return faces_result


def fetch_s3objects(bucket_name:str, prefix:str, objects_list=None) -> Dict:
    """ return dict of s3objects """
    bucket = get_bucket(bucket_name)
    if not prefix.endswith('/'):
        prefix = prefix+'/'
    if not objects_list:
        objects_list = bucket.objects.filter(Prefix=prefix, )
    mydict = {
        object.key : object 
            for object in objects_list
                if not object.key.endswith('/')
    }
    return mydict


def join_s3_part_objects(bucket, prefix):
    """ return s3objects joined with faces. """
    s3objects = fetch_s3objects(bucket, prefix)
    faces = FaceAssetsDb.scan()
    result = []
    for face in faces:
        for fpart in face.parts:
            s3_key = fpart.s3_key
            if s3_key in s3objects:
                ''' file exists '''
                s3obj = s3objects[s3_key]
                # add reference to the part to compare differences between
                #   s3 and the database.
                s3obj.part = fpart
                # part_date = fpart.updateDate if fpart.updateDate else fpart.createDate
                # if not part_date:
                if s3obj.e_tag != fpart.etag:
                    s3obj.status = 'modified'
                elif s3obj.e_tag == fpart.etag:
                    s3obj.status = 'nochange'
                else:
                    s3obj.status = 'unknown'
                ### end set status
                s3obj.version = fpart.version
                del s3objects[s3_key]
            else:   
                ''' file deleted '''
                s3obj = types.SimpleNamespace(
                    key = s3_key,
                    part = fpart,
                    status = 'deleted'
                )
            result.append(s3obj)

    for key, obj in list(s3objects.items()):
        ''' remaining s3objects are all new files then '''
        obj.status = 'new'
        obj.part = None
        obj.version = None
    
    result = result + list(s3objects.values())
    result_map = {
        obj.key : obj
            for obj in sorted(result, key=lambda a: a.key)
    }
    return result_map


def copy_s3_objects(bucket, prefix, paths, to_bucket=BUCKET_NAME, to_prefix=BUCKET_PREFIX):
    """ Copy s3 objects """
    from_bucket = bucket
    from_prefix = prefix
    log.debug(f'CopyS3Objects bucket:{from_bucket} paths length:{len(paths)}')
    to_bucketobj = get_bucket(to_bucket)
    errors = []
    for path in paths:
        from_key = os.path.join(from_prefix, path)
        source = {'Bucket':from_bucket, 'Key':from_key}
        to_key = os.path.join(to_prefix, path)
        log.debug(f'copying... from_key:{from_key} to_key:{to_key}')
        try:
            to_bucketobj.copy(source, to_key)
        except Exception as ex:
            errors.append(path)
    if len(errors) == 0:
        return None
    else:
        return errors

    # copy_source = {
    #     'Bucket': 'mybucket',
    #     'Key': 'mykey'
    # }
    # bucket = s3.Bucket('otherbucket')
    # bucket.copy(copy_source, 'otherkey')


def sync_s3_objects(bucket, prefix, to_bucket=BUCKET_NAME, to_prefix=BUCKET_PREFIX):
    ''' Sync S3 objects between two buckets '''
    from_bucket = get_bucket(bucket)
    from_prefix = prefix
    to_bucket = get_bucket(to_bucket)

    from_etags = {}
    for obj in from_bucket.objects.filter(Prefix=from_prefix):
        key = grep_prefix_key(obj.key, from_prefix)
        log.debug(f'from: {key}')
        from_etags[key] = obj.e_tag
    to_etags = {}
    for obj in to_bucket.objects.filter(Prefix=to_prefix):
        key = grep_prefix_key(obj.key, to_prefix)
        log.debug(f'to: {key}')
        to_etags[key] = obj.e_tag

    from_diff = dict(set(from_etags.items())  - set(to_etags.items()))

    return from_etags,to_etags,from_diff

    # get from etags from bucket
    # get to etags to bucket
    # to_diff = to - from
    # from_diff = from - to


def testme(count=100):
    for i in range(count):
        log.debug('before query...')
        marks = list(FaceMarksDb.markVersionIdIndex.query(hash_key='0019baf7-b699-4899-8962-abf2775f323a'))
    log.debug('done')


if __name__ == '__main__':
    # handler = logging.StreamHandler()
    # handler.setLevel(logging.DEBUG)
    # log.addHandler(handler)
    # log.setLevel(logging.DEBUG)

    #process_face_container('testwebapp-userfiles-mobilehub-1585053506', prefix='public/FaceAssets')
    #process_face_container2('withmelabs-faces', prefix='FaceAssets', faces=list(FaceAssetsDb.scan()))
    # process_face_asset('withmelabs-faces', prefix='FaceAssets/Facemaker')
    # process_face_container('withmelabs-faces', prefix='FaceAssets')
    #add_mark('eed10c0c-766e-4ee1-a173-65c9dd27c162', 'withmelabs-faces', prefix='FaceAssets')
    testme()
    pass


def mycreate_tables():
    FaceAssetsDb.mycreate_table()
    FacePartsDb.mycreate_table()
    FaceMarksDb.mycreate_table()

def mydelete_tables():
    FacePartsDb.delete_table()
    FaceAssetsDb.delete_table()
    FaceMarksDb.delete_table()
 
