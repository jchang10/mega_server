
from flask_bootstrap import Bootstrap
bootstrap = Bootstrap()

from flask_login import LoginManager
login_manager = LoginManager()
login_manager.login_view = 'login'

from flask_cors import CORS
cross_origin_resource_sharing = CORS()

from flask_marshmallow import Marshmallow
marshmallow = Marshmallow()

from app.extensions import api
from app.extensions import auth
from app.extensions import cognito
from app.extensions import logging

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
db = SQLAlchemy()
migrate = Migrate()

def init_app(app):
    """
    Application extensions initialization.
    """
    for extension in (
            bootstrap,
            login_manager,
            cross_origin_resource_sharing,
            marshmallow,
            api,
            auth,
            cognito,
    ):
        extension.init_app(app)

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    migrate.init_app(app, db)
