
import os, sys
from app import create_app

"""
python3 run.py
"""
import config
app = create_app()


if __name__ == '__main__':
    # with app.app_context():
        #db.create_all()
        # # create a development user
        # if User.query.get(1) is None:
        #     u = User(username='john')
        #     u.set_password('cat')
        #     db.session.add(u)
        #     db.session.commit()
    #app.run(port=5000)
    #app.run(ssl_context='adhoc')

    app.run(ssl_context=('config/cert.pem','config/key.pem'))
