from botocore.exceptions import ClientError

from flask import g, request

from flask_restplus_patched import Namespace, Resource
from flask_restplus_patched._http import HTTPStatus

from app.modules.auth.resources import authenticate
from .models import User
from .schemas import UserSchema, GroupSchema

api = Namespace('users', description='Users description')


def get_user_by_userid(user_id):
    try:
        user = User.get_user(username=user_id)
    except ClientError as e:
        api.abort(404,
                  status='fail',
                  message='User with given id was not found.')
    groups_response = User.cognito.admin_list_groups_for_user(user._metadata['username'])
    user.groups_response = groups_response.get('Groups',[])
    response_object = user
    return response_object


@api.route('/ping')
class Ping(Resource):
    def get(self):
        return dict(hello='world')

    
@api.route('/appsynctest')
class AppSync(Resource):
    def get(self):
        return [ dict(one='oneval',two='twoval'),
                 dict(one='threeval',two='fourval') ]

    
@api.route('/')
class UsersResource(Resource):
    # def get(self):
    #     warrant = User.warrant()
    #     users = warrant.get_users()
    #     return {
    #         'status': 'success',
    #         'data': {
    #             'users': api.marshal(users, user_model)
    #         }
    #     }
    @authenticate(admin=True)
    @api.doc(security='token')
    @api.response(UserSchema(many=True, exclude=('is_admin','groups')))
    def get(self):
        warrant = User.warrant()
        users = warrant.get_users()
        return users
    

@api.response(
    code=HTTPStatus.FORBIDDEN,
    description="Authorization Token is missing."
)
@api.response(
    code=HTTPStatus.NOT_FOUND,
    description="User not found.",
)
@api.route('/<user_id>')
class UsersbyUserIdResource(Resource):
    @api.response(UserSchema())
    def get(self, user_id):
        return get_user_by_userid(user_id)


@api.response(
    code=HTTPStatus.FORBIDDEN,
    description="Authorization Token is missing."
)
@api.response(
    code=HTTPStatus.NOT_FOUND,
    description="User not found.",
)
@api.route('/me')
class UsersMeResource(Resource):
    @authenticate()
    @api.response(UserSchema())
    def get(self):
        """
        Me User Record
        Look up our own user record.
        """
        userid = g.current_user.username
        return get_user_by_userid(userid)


