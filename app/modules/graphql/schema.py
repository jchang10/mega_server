import graphene
import graphene.types.datetime


class UserInterface(graphene.Interface):
    username = graphene.String()
    email = graphene.String()
    #maybe do not include enabled or create_date?
    #only provided from cognito admin_get_user() call.
    enabled = graphene.Boolean()
    create_date = graphene.types.datetime.DateTime()

    # See for resolve_type:
    # https://github.com/graphql-python/graphene/issues/699#issuecomment-378751225