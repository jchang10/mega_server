import boto3, datetime, logging, os, uuid
from typing import Dict, Iterator, List, Tuple

from app.modules.faces.s3 import get_bucket
from app.modules.faces.models import FaceAssetsDb, FacePartsDb, FaceMarksDb
from app.modules.s3faces.services import grep_prefix_key

log = logging.getLogger(__name__)


def validate_mark(mark_id:str) -> FaceMarksDb:
    ''' Set is_valid flag on given FaceMarksDb '''
    mark = FaceMarksDb.get(mark_id)
    if mark:
        mark.is_valid = True
        mark.save()
    return mark


def publish_mark(mark_id:str) -> FaceMarksDb:
    ''' Set is_publish flag on given FaceMarksDb '''
    mark = FaceMarksDb.get(mark_id)
    if mark and mark.is_valid:
        mark.is_publish = True
        mark.save()
    return mark


def add_mark2(bucket_name:str, prefix:str, dryrun:bool, compare_mark:FaceMarksDb=None) \
        -> Tuple[FaceMarksDb, str, Dict, Dict]:
    ''' Scan through all s3 object_versions and save the latest version_id's in a map
        Return: [FaceMarksDb, bool -> whether this is a new mark or not] '''
    mark = FaceMarksDb(
        str(uuid.uuid4()), 
        typeKey = FaceMarksDb.MARK_TYPE,
        bucket = bucket_name, 
        prefix = prefix,
        createDate = datetime.datetime.utcnow()
    )
    bucket = get_bucket(bucket_name)
    partmap = mark.partVersions
    object_versions = bucket.object_versions.filter(Prefix=prefix)
    part_marks = {} # save parts here to save later
    deleted_marks = {} # save deleted versions 

    for version in object_versions.all():
        log.debug(f'key:{version.key} is_latest:{version.is_latest}')
        if version.key.endswith('/'):
            continue
        part_key = grep_prefix_key(version.key, prefix)
        if version.is_latest:
            if version.size and version.storage_class:
                # delete_marker is indicated by version.size version.etag or version.storage_class is None
                if version.key:
                    log.debug(f'saving key:{version.key} version_id:{version.version_id}')
                    partmap[part_key] = version.version_id
                    # load the MarkPartsIndex
                    mark_part = FaceMarksDb(
                        str(uuid.uuid4()),
                        typeKey = FaceMarksDb.PART_TYPE,
                        markId = mark.uuid,
                        partKey = part_key,
                        versionId = version.version_id,
                        createDate = datetime.datetime.utcnow()
                    )
                    # mark_part.save()
                    # batch.save(mark_part)
                    part_marks[part_key] = mark_part
            else:
                # delete marker
                deleted_marks[part_key] = FaceMarksDb(
                    None,
                    typeKey = FaceMarksDb.PART_TYPE,
                    markId = None,
                    partKey = part_key,
                    versionId = version.version_id,
                    createDate = datetime.datetime.utcnow()
                )
        else:
            # version history
            pass
            # if dryrun == False:
            #     # check if this versionId is being used
            #     marks = list(FaceMarksDb.markVersionIdIndex.query(hash_key=version.version_id))
            #     if len(marks) == 0 and dryrun == False:
            #         version.delete()
            #         log.debug(f'deleting unused versionId:{version.version_id}')

    # mark.save()
    # batch.save(mark)

    # filter versions that do not have a corresponding FacePartsDb
    parts:Dict = {}
    for key in list(part_marks.keys()):  # list because modifying part_marks
        _parts = list(FacePartsDb.partKeyIndex.query(key))
        if len(_parts):
            parts[key] = _parts[0]
            parts[key].set_mark(part_marks[key])
        else:
            del part_marks[key]        
    for key in list(deleted_marks.keys()):  # list because modifying part_marks
        _parts = list(FacePartsDb.partKeyIndex.query(key))
        if len(_parts):
            parts[key] = _parts[0]
            parts[key].set_mark(deleted_marks[key])
        else:
            del deleted_marks[key]

    # only save this new Mark, if there are changes from the previous one or
    # the given compare_mark.
    last_mark = compare_mark if compare_mark else FaceMarksDb.query_last()
    if last_mark:
        last_marks_map = { mark.partKey : mark
                for mark in FaceMarksDb.markPartsIndex.query(last_mark.uuid)
        }
        last_versions_map = { key : part.versionId 
                for key, part in last_marks_map.items() 
        }
    else:
        last_versions_map = {}

    versions_map = { key: part.versionId
        for key, part in part_marks.items()
    }

    # compare versions_map with last_versions_map. only save the new Mark
    #   if the versions_map is different.
    diff_map = dict(set(versions_map.items()) - set(last_versions_map.items()))
    diff_parts = { key : parts[key] for key in diff_map }
    deleted_parts = { key : parts[key] for key in deleted_marks }

    if last_mark == None or diff_map or deleted_parts:
        # if anything has changed, return the new Mark
        with FaceMarksDb.batch_write() as batch:
            if dryrun == False: batch.save(mark)
            for key, part in part_marks.items():
                if dryrun == False: batch.save(part)

        return mark, 'new', diff_parts, deleted_parts
    else:
        # no changes. return old mark.
        return last_mark, 'old', diff_parts, deleted_parts


def diff_marks(old_mark_id: str, new_mark_id: str) -> \
    Tuple[FaceMarksDb, Dict[str, FacePartsDb], Dict[str, FacePartsDb]]:
    """ Compute the diff and deleted parts. """
    old_mark = FaceMarksDb.get(old_mark_id)
    assert old_mark, "old_mark_id does not exist!"
    new_mark = FaceMarksDb.get(new_mark_id)
    assert new_mark, "new_mark_id does not exist!"
    old_versions_map:Dict = {}
    old_marks = FaceMarksDb.markPartsIndex.query(old_mark_id)
    old_marks_map = { mark.partKey : mark
        for mark in old_marks
    }
    old_versions_map = { mark.partKey : mark.versionId
        for mark in old_marks_map.values()
    }
    new_marks = FaceMarksDb.markPartsIndex.query(new_mark_id)
    new_marks_map = { mark.partKey : mark
        for mark in new_marks
    }
    new_versions_map = { mark.partKey : mark.versionId
        for mark in new_marks_map.values()
    }
    diff_map = dict(set(new_versions_map.items()) - set(old_versions_map.items()))
    diff_parts: Dict = {}
    for key in diff_map:
        parts = list(FacePartsDb.partKeyIndex.query(key))
        if len(parts):
            part = parts[0]
            part.set_mark(new_marks_map[key])
            diff_parts[key] = part
    deleted = set(old_versions_map) - set(new_versions_map)
    deleted_parts: Dict = {}
    for key in deleted:
        parts = list(FacePartsDb.partKeyIndex.query(key))
        if len(parts):
            part = parts[0]
            part.set_mark(old_marks_map[key])
            deleted_parts[key] = part
    return new_mark, diff_parts, deleted_parts


def admin_purge_unused_versions(bucket:str, prefix:str, dryrun:bool) -> List:
    """ admin function. delete from s3 all unused versions. """
    bucketobj = get_bucket(bucket)
    object_versions = bucketobj.object_versions.filter(Prefix=prefix)

    marks = {}
    for mark in FaceMarksDb.scan():
        key1 = f'{prefix}{mark.partKey}/{mark.versionId}'
        key2 = f'{prefix}{mark.partKey}'
        marks[key1] = mark
        marks[key2] = mark

    deleted_versions:List = []
    for version in object_versions:
        key1 = f'{version.key}/{version.version_id}'
        key2 = version.key
        log.debug(f'key1:{key1}')
        if version.is_latest:
            """ keep all latest versions """
            if not version.size and not version.storage_class:
                """ except if this is a delete marker. if there are no other versions that exist,
                    then deleting this version, will effectively delete the entire file, which is
                    probably what we want. if there are other versions, then we need to keep this
                    delete marker around, otherwise, we would effectively be un-deleting this file. """
                if not key2 in marks:
                    """ we can permanently delete this file. """
                    log.info('permanently deleting this file key:'+key2)
                    version.delete()
                    deleted_versions.append(version)
        else:
            """ if an older version, delete unused versions """
            log.debug(f'KEY:{key1}')
            if not key1 in marks:
                log.info(f'deleting unused key:{version.key} version:{version.version_id}...')
                version.delete()                        
                deleted_versions.append(version)
                
    return deleted_versions


def join_parts_marks(mark:FaceMarksDb, face:FaceAssetsDb) -> Dict[str, FacePartsDb]:
    """ Join FacePartsDb with associated FaceMarksDb. """
    part_marks = {
        _mark.partKey : _mark
            for _mark in FaceMarksDb.markPartsIndex.query(mark.uuid)
    }
    parts = face.query_parts_map(deleted=True)
    for key, part in list(parts.items()):
        if key in part_marks:
            part.set_mark(part_marks[key])
        else:
            del parts[key]            
    return parts


def join_hair_parts_marks(mark:FaceMarksDb, face:FaceAssetsDb=None) -> Dict[str, List[FacePartsDb]]:
    """ Join FacePartsDb with associated FaceMarksDb. """
    part_marks = {
        _mark.partKey : _mark
            for _mark in FaceMarksDb.markPartsIndex.query(mark.uuid)
    }
    parts = face.query_parts_map(deleted=True)
    for key, part in list(parts.items()):
        if key in part_marks and \
            parts[key].part == 'hair':
            part.set_mark(part_marks[key])
        else:
            del parts[key]            
    parts2:Dict[str, List] = {}
    for key, part in parts.items():
        name = os.path.splitext(key)[0]
        name = os.path.basename(name)
        parts2.setdefault(name, []).append(part)
    return parts2