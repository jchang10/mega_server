import pytest

args = ["mytests", "pytests",
        "-o", "cache_dir=/tmp"
]

def main():
    pytest.main(args)

if __name__ == '__main__':
    main()
