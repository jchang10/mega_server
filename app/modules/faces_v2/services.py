from flask import g
from typing import List, Tuple, Dict, Any
import boto3, datetime, logging, os, re, uuid

from app.modules.faces_v2.models import FaceDb, PartDb
from app.modules.faces.s3 import BUCKET_NAME, BUCKET_PREFIX, get_bucket
from app.extensions import db

log = logging.getLogger(__name__)


_PARTS = ['hair','meshes','shaders','textures']
#_EXTS_INCLUDE = ['.fbx','.png']
_EXTS_INCLUDE:List[str] = ['']
_EXTS_EXCLUDE:List[str] = []

def grep_part(rest:str) -> str:
    """ determine if any part names are found """
    for part in _PARTS:
        if rest.find(part) > -1:
            return part
    return None

def grep_gender(rest:str) -> str:
    if rest.find('female') > -1:
        return 'female'
    elif rest.find('male') > -1:
        return 'male'
    else:
        return None

def grep_prefix_key(key:str, prefix:str) -> str:
    return key[len(prefix)+1:] # remove prefix

def grep_face_path(path:str):
    """ return the bucket/prefix/facename path only. strip the part path. """
    face_path = re.sub(f'(^{BUCKET_NAME}/{BUCKET_PREFIX}\/[^\/]+)/(.*)', '\g<1>', path)
    return face_path

def filter_exts(part_path:str) -> bool:
    if not any(part_path.lower().endswith(s) for s in _EXTS_INCLUDE):
        log.info('Not in _EXTS_INCLUDE "part_path". skipping...')
        return True

    if any(part_path.lower().endswith(s) for s in _EXTS_EXCLUDE):
        log.info('Exists _EXTS_EXCLUDE in "part_path". skipping...')
        return True
    
    return False

def process_face_asset(
        bucket_name:str, 
        prefix:str, 
        objects_list = None
    ) -> FaceDb:
    """ Process S3 files. Start at the given path."""

    # re-arrange vars here
    full_face_path = f'{bucket_name}/{prefix}'
    face_path = grep_face_path(full_face_path)
    face_name = os.path.basename(face_path)
    face_prefix = os.path.dirname(face_path)

    face = FaceDb.get_or_create(face_path)

    bucket = get_bucket(bucket_name)
    if objects_list == None:
        objects_list = bucket.objects.filter(Prefix=prefix)

    for obj in objects_list:
        log.debug(f'key:{obj.key} size:{obj.size}')
        part_path = grep_prefix_key(obj.key, face.prefix)

        if not face.prefix in obj.key:
            """ ensure the part matches the face """
            break

        # include and exclude filters
        if filter_exts(part_path):
            continue

        # try assigning some fields
        owner = g.current_user.username if g.current_user else None
        size = obj.size
        part_name = grep_part(part_path)
        gender = grep_gender(part_path)
    
        part = face.parts_map.get(part_path)
        if not part:
            part = PartDb(uuid=uuid.uuid4(), face=face, path=part_path)

        # part.set_attributes(**dict(
        #     size = size,
        #     gender = gender,
        #     part = part_name,
        #     owner = owner
        #     # owner = 'owner_tbd',
        #     # #region # handle versioning
        #     # version_id = latest_version_id
        #     # #endregion
        # ))
        log.debug('saving... '+part.path)
        db.session.add(part)

    db.session.add(face)
    db.session.commit()
    return face
    

