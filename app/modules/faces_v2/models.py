import datetime, os, uuid
from app.extensions import db
from sqlalchemy_utils import UUIDType
from sqlalchemy.orm.collections import attribute_mapped_collection


class VersionedMixin:
    """Adds `created` and `updated` columns to a derived declarative model.

    The `created` column is handled through a default and the `updated`
    column is handled through a `before_update` event that propagates
    for all derived declarative models.

    ::


        import sqlalchemy as sa
        from sqlalchemy_utils import Timestamp


        class SomeModel(Base, Timestamp):
            __tablename__ = 'somemodel'
            id = sa.Column(sa.Integer, primary_key=True)
    """

    version = db.Column(db.Integer)
    # create_date = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)
    # update_date = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)
    create_date = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    update_date = db.Column(db.DateTime)

@db.event.listens_for(VersionedMixin, 'before_update', propagate=True)
def timestamp_before_update(mapper, connection, target):
    # When a model with a timestamp is updated; force update the updated
    # timestamp.
    target.version = target.version + 1
    target.update_date = datetime.datetime.utcnow()
    

class FaceDb(VersionedMixin, db.Model):
    __tablename__ = 'faces'
    uuid = db.Column(UUIDType(binary=False), primary_key=True)
    name = db.Column(db.String(1000))
    path = db.Column(db.String(1000))

    parts = db.relationship('PartDb', backref='face')
    parts_map = db.relationship('PartDb',
        collection_class = attribute_mapped_collection('path'),
        cascade = 'all, delete-orphan')

    _bucket = None
    _prefix = None

    @property
    def bucket(self) -> str:
        if not self._bucket:
            (self._bucket, self._prefix) = self.path.split('/', 1)
        return self._bucket

    @property
    def prefix(self) -> str:
        if not self._prefix:
            (self._bucket, self._prefix) = self.path.split('/', 1)
        return self._prefix

    @property
    def face_name(self) -> str:
        return os.path.basename(self.path)

    @classmethod
    def get_or_create(cls, path:str) -> 'FaceDb':
        face = cls.query.filter_by(path=path).first()
        if face: #exists
            face.isnew = False
        else: #new
            face = cls(
                uuid=str(uuid.uuid4()),
                path = path,
            )
            face.isnew = True
        return face
    


class PartDb(VersionedMixin, db.Model):
    __tablename__ = 'parts'
    uuid = db.Column(UUIDType(binary=False), primary_key=True)
    face_uuid = db.Column(UUIDType(binary=False),db.ForeignKey('faces.uuid'))
    path = db.Column(db.String(1000))

    _version_id = None

    @classmethod
    def get_or_create(cls, face:'FaceDb', path:str) -> 'PartDb':
        part = cls.query.filter_by(
            face_uuid = face.uuid,
            path = path
        ).first()
        if part: #exists
            part.isnew = False
        else: #new
            part = cls(
                uuid = str(uuid.uuid4()),
                face_uuid = face.uuid,
                path = path,
                face = face
            )
            part.isnew = True
        return part

    @property
    def s3_key(self:'PartDb') -> str:
        assert self.face, "face does not exist"
        s3_key = f'{self.face.prefix}{self.path}'
        return s3_key
