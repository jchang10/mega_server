
from app.extensions.api import api_v1

def init_app(app):
    from . import resources, views
    app.register_blueprint(views.users_blueprint)
    api_v1.add_namespace(resources.api)


#exports
# from .models import User
# from .schemas import UserSchema
# from .graphql import UserInterface

