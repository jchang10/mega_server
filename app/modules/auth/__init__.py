
from app.extensions.api import api_v1

def init_app(app):
    # Touch underlying modules
    #from . import models, views, resources  # pylint: disable=unused-variable
    from . import views, resources
    
    # Mount authentication routes
    app.register_blueprint(views.auth_blueprint)
    api_v1.add_namespace(resources.api)


#exports
