
from flask import g
from flask_restplus_patched import Namespace, Resource

from app.modules.auth.resources import authenticate
from .models import NotesDb
from .schemas import NotesDbSchema, AddNotesParameters

api = Namespace('notes', description='Notes description')


@api.route('/notesdb')
class DatabaseResource(Resource):
    @authenticate()
    @api.response(NotesDbSchema())
    def get(self):
        """
        Testing PynamoDB. Notes are saved per-user.
        """
        user_id = g.current_user.username
        try:
            note = NotesDb.get(user_id)
        except:
            note = NotesDb(user_id)
            note.save()
        return note


    @authenticate()
    @api.parameters(AddNotesParameters())
    @api.response(NotesDbSchema())
    def post(self, args):
        """
        Update the Note
        """
        user_id = g.current_user.username
        try:
            note = NotesDb.get(user_id)
        except:
            note = NotesDb(user_id)
            note.save()
        vars(note)['attribute_values'].update(args)
        note.save()
        return note


'''
from flask_marshmallow import Schema, base_fields
class TestSchema(Schema):
    """ Possible way of enveloping another schema """
    status = base_fields.Str()
    data = base_fields.Method('get_data')
    userdb = base_fields.Nested(UserDbSchema)

    def get_data(self, obj):
        return obj


    
'''
