# required for mypy to work without complaining

from .models import FaceAssetsDb, FacePartsDb, FaceMarksDb

if not FaceAssetsDb.exists():
    print('creating FaceAssets dynamodb table')
    FaceAssetsDb.mycreate_table()

if not FacePartsDb.exists():
    print('creating FaceParts dynamodb table')
    FacePartsDb.mycreate_table()

if not FaceMarksDb.exists():
    print('creating FaceMarks dynamodb table')
    FaceMarksDb.mycreate_table()
