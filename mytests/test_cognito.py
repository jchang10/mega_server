import json
import datetime

from flask import url_for

from mytests.base import BaseTestCase


class TestCognitoService(BaseTestCase):
    """Tests for the Cognito Service."""

    def test_ping(self):
        """Ensure the /ping route behaves correctly."""
        response = self.client.get('/api/v1/cognito/ping')
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('pong!', data['message'])
        self.assertIn('success', data['status'])


    def test_admin_get_user_with_valid_user(self):
        ''' Test logging in '''
        with self.client:
            # user login
            self.create_admin_test_user('jc-test', 'Test User', 'Test2test', email='jc-test@jline.com')
            resp_login = self.client.post(
                '/api/v1/auth/login',
                data=json.dumps(dict(
                    username='jc-test',
                    password='Test2test'
                )),
                content_type='application/json'
            )
            # valid token logout
            response = self.client.get(
                '/api/v1/cognito/admin_get_user/jc-test',
                headers=dict(
                    Authorization='Bearer ' + json.loads(
                        resp_login.data.decode()
                    )['auth_token']
                )
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual(data['data']['username'], 'jc-test')
            self.assertEqual(data['data']['email'], 'jc-test@jline.com')
            self.assertIn('success', data['status'])


    def test_admin_get_user_with_invalid_user(self):
        ''' Test logging in '''
        with self.client:
            # user login
            self.create_admin_test_user('jc-test', 'Test User', 'Test2test', email='jc-test@jline.com')
            resp_login = self.client.post(
                '/api/v1/auth/login',
                data=json.dumps(dict(
                    username='jc-test',
                    password='Test2test'
                )),
                content_type='application/json'
            )
            auth_token = json.loads(resp_login.data.decode()).get('auth_token')
            # valid token logout
            response = self.client.get(
                '/api/v1/cognito/admin_get_user/jc-test1',
                headers=dict(Authorization='Bearer ' + auth_token)
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('User does not exist', data['message'])
            self.assertIn('fail', data['status'])


    def test_get_user(self):
        ''' Test logging in '''
        with self.client:
            # user login
            self.create_test_user('jc-test', 'Test User', 'Test2test', email='jc-test@jline.com')
            resp_login = self.client.post(
                '/api/v1/auth/login',
                data=json.dumps(dict(
                    username='jc-test',
                    password='Test2test'
                )),
                content_type='application/json'
            )
            # valid token logout
            response = self.client.get(
                '/api/v1/cognito/get_user',
                headers=dict(
                    Authorization='Bearer ' + json.loads(
                        resp_login.data.decode()
                    )['auth_token']
                )
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual(data['data']['email'], 'jc-test@jline.com')
            self.assertFalse(data['data']['is_admin'])
            self.assertIn('success', data['status'])
            
