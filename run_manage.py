import os, sys, unittest, coverage
from flask_script import Manager, Shell, Server
from app import create_app

COV = coverage.coverage(
    branch=True,
    include='*',
    omit=[
        'mytests/*',
        'venv/*',
        '/usr/local/*'
    ]
)
COV.start()

app = create_app()
manager = Manager(app)
manager.add_command('runserver', Server(ssl_crt='config/cert.pem', ssl_key='config/key.pem'))

from app.modules.users.models import User

@manager.shell
def make_shell_context():
    return dict(app=app, User=User, display_config=display_config)


@manager.command
def test(test_name=None):
    """Runs the unit tests without test coverage."""
    if test_name is None:
        tests = unittest.TestLoader().discover('mytests', pattern='test_*.py')
    else:
        tests = unittest.TestLoader().loadTestsFromName('mytests.'+test_name)
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


from app.modules.commands import commands

@manager.command
def db(dump='table', load='table'):
    if dump != 'table':
        table_name = dump
        commands.db_dump(table_name)
    elif load != 'table':
        table_name = load
        commands.db_load(table_name)


@manager.command
def cov():
    """Runs the unit tests with coverage."""
    tests = unittest.TestLoader().discover('mytests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        COV.stop()
        COV.save()
        print('Coverage Summary:')
        COV.report()
        COV.html_report()
        COV.erase()
        return 0
    return 1


def display_config():
    print(f'User.exists: {User.exists()}')
    print(f'User.region: {User.Meta.region}')


if __name__ == '__main__':
    manager.run()

