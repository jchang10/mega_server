from flask import Flask, g, render_template, request, redirect, url_for, current_app as app
from flask import Blueprint
from flask_login import login_user, login_required, current_user, logout_user
from flask_wtf import FlaskForm
import wtforms

users_blueprint = Blueprint('users', __name__, url_prefix='/users')


class NotesForm(FlaskForm):
    note1 = wtforms.StringField('note1')
    note2 = wtforms.StringField('note2')
    note3 = wtforms.StringField('note3')

    
@users_blueprint.route('/ping')
def ping():
    return 'hello world'


@users_blueprint.route('/notes', methods=['GET','POST'])
def notes():
    form = NotesForm()
    return render_template('notes.html', form=form)
    
