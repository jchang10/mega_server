import logging
from logging.config import dictConfig

dev_logging_config = dict(
    disable_existing_loggers=False,
    version = 1,
    # handlers = {
    #     'console':{
    #         'class':'logging.StreamHandler'
    #     }
    # },
    loggers = {
        #always print __main__ which are used when testing modules on command line
        '__main__':{
            # 'handlers':['console'],
            'level':'DEBUG'
        },
        'flask_graphql.graphqlview':{
            # 'handlers':['console'],
            'level':'DEBUG'
        },
        'app.modules.s3faces.s3process':{
            'level':'DEBUG'
        },
        'app.testme.faces.s3process':{
            'level':'DEBUG'
        },
        'app':{
            'level':'DEBUG'
        },
    }
)

logging_config = dict(
    disable_existing_loggers=False,
    version = 1,
    # handlers = {
    #     'console':{
    #         'class':'logging.StreamHandler'
    #     }
    # },
    loggers = {
        #always print __main__ which are used when testing modules on command line
        '__main__':{
            # 'handlers':['console'],
            'level':'DEBUG'
        },
        'flask_graphql.graphqlview':{
            # 'handlers':['console'],
            'level':'DEBUG'
        },
        'app.modules.s3faces.s3process':{
            'level':'DEBUG'
        },
        'app.testme.faces.s3process':{
            'level':'DEBUG'
        },
        'app':{
            'level':'DEBUG'
        },
        'botocore':{
            # not sure why but AWS Lambda sets this to wordy DEBUG 
            'level':'INFO'
        }
    }
)

import config
config_name = config.config.get_stage()

logging.basicConfig()
if config_name == 'dev':
    dictConfig(dev_logging_config)
else:
    dictConfig(logging_config)
#Turns on all logging
#logging.basicConfig(level=logging.DEBUG,
#logging.getLogger('flask_graphql.graphqlview').setLevel(logging.DEBUG)

# def init_app(app):
#     pass
