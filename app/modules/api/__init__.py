
from flask import Blueprint
from app.extensions.api import api_v1

def init_app(app):

    api_v1_blueprint = Blueprint('api', __name__, url_prefix='/api/v1')
    api_v1.init_app(api_v1_blueprint)
    app.register_blueprint(api_v1_blueprint)


