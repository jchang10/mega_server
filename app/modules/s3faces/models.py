import boto3, logging, os, re
from typing import Dict, List
from pynamodb.attributes import UnicodeAttribute, BooleanAttribute, UTCDateTimeAttribute, NumberAttribute, MapAttribute
from pynamodb.indexes import GlobalSecondaryIndex, KeysOnlyProjection, AllProjection
import pynamodb.models
from app.modules.faces.models import VersionedMixin, BaseMixin, BaseMeta

log = logging.getLogger(__name__)



