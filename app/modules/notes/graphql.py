from botocore.exceptions import ClientError

import graphene
from graphene_pynamodb import PynamoObjectType
from graphql import GraphQLError

from .models import NotesDb
from app.modules.auth.resources import current_user
from app.modules.users.models import User
from app.modules.users.graphql import UserInterface
from app.modules.cognito.graphql import CognitoUser


class NotesType(PynamoObjectType):
    class Meta:
        model = NotesDb
        interfaces = (graphene.Node,)

    user = graphene.Field(UserInterface)

    def resolve_user(self, info, **args):
        try:
            user = User.get_user(username=self.user_id)
        except ClientError as ex:
            return None
        return user


class NotesQuery(graphene.AbstractType):
    notes = graphene.List(NotesType)
    note = graphene.Field(NotesType,
                          user_id = graphene.String()
    )

    def resolve_note(self, info, **args):
        user_id = args.get('user_id')
        assert user_id, 'user_id not given'
        note = NotesDb.get(user_id)
        return note

    def resolve_notes(self, info, **args):
        # if current_user.is_admin:
        #     return list(NotesDb.scan())
        # elif current_user.username != 'unauthorized':
        #     return [NotesDb.get(current_user.username)]
        # else:
        #     return None
        return list(NotesDb.scan())


class AddNote(graphene.ClientIDMutation):
    note = graphene.Field(NotesType)
    
    class Input:
        user_id = graphene.String()
        note1 = graphene.String()
        note2 = graphene.String()
        note3 = graphene.String()
        key = graphene.String()
        value = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **args):
        user_id = args.get('user_id')        
        key = args.get('key')
        value = args.get('value')
        if not current_user.is_admin or user_id == '':
            user_id = current_user.sub
        try:
            note = NotesDb.get(user_id)
            raise GraphQLError(f'user_id ({user_id}) already exists')
        except:
            pass
        note = NotesDb(
            user_id = user_id,
            note1 = args.get('note1'),
            note2 = args.get('note2'),
            note3 = args.get('note3'),
        )
        setattr(note,key,value)
        note.save()
        return AddNote(note=note)


class UpdateNote(graphene.ClientIDMutation):
    note = graphene.Field(NotesType)

    class Input:
        user_id = graphene.String()
        note1 = graphene.String()
        note2 = graphene.String()
        note3 = graphene.String()
        key = graphene.String()
        value = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **args):
        note = NotesDb.get(args.get('user_id'))
        key = args.get('key')
        value = args.get('value')
        # if args.get('note1'): note.note1 = args.get('note1')
        # if args.get('note2'): note.note2 = args.get('note2')
        # if args.get('note3'): note.note3 = args.get('note3')
        setattr(note, key, value)
        note.save()
        return UpdateNote(note=note)


class DeleteNote(graphene.ClientIDMutation):
    note = graphene.Field(NotesType)

    class Input:
        user_id = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **args):
        note = NotesDb.get(args.get('user_id'))
        if note:
            note.delete()
        return DeleteNote(note=note)


class NotesMutation(graphene.AbstractType):
    add_Note = AddNote.Field()
    update_Note = UpdateNote.Field()
    delete_Note = DeleteNote.Field()


