
import pynamodb
import json, time, os
from typing import List, cast, Any
from app.modules.faces.models import FaceAssetsDb, FacePartsDb, FaceMarksDb

from app.extensions.logging import logging
log = logging.getLogger(__name__)

BACKUP_DIR='db_backup'

def verify_backup_dir():
    if os.path.exists(BACKUP_DIR):
        return True
    else:
        os.mkdir(BACKUP_DIR)
    return True

def backup_filename(table_name:str):
    verify_backup_dir()
    return f'{BACKUP_DIR}/{table_name}.backup'

TABLES:List[Any] = [FaceAssetsDb, FacePartsDb, FaceMarksDb]

def db_dump(table_name:str):
    verify_backup_dir()
    for t in TABLES:
        target_table_name = t.Meta.table_name
        if table_name == 'all' or target_table_name == table_name:
            filename = backup_filename(target_table_name)
            log.debug('dump table backup to file ... '+filename)
            model_dump(t, filename)

def db_load(table_name:str):
    for t in TABLES:
        target_table_name = t.Meta.table_name
        if table_name == 'all' or target_table_name == table_name:
            filename = backup_filename(target_table_name)
            if os.path.exists(filename):
                log.debug('load table backup from file ...'+filename)
                model_load(t, filename)

def model_dump(model, filename):
    with open(filename, 'w') as out:
        for item in model.scan():
            line = json.dumps(item._get_json())
            out.write(line+'\n')

def model_loads(model, data):
    content = json.loads(data)
    with model.batch_write() as batch:
        item = model._from_data(content)
        batch.save(item)

def model_load(model, filename):
    with open(filename, 'r') as inf:
        for i, line in enumerate(inf):
            log.debug(f'loading line {i}')
            model_loads(model, line)

import sys

def get_disabled(self):
    return self._disabled

def set_disabled(self, disabled):
    frame = sys._getframe(1)
    if disabled:
        print('{}:{} disabled the {} logger'.format(
            frame.f_code.co_filename, frame.f_lineno, self.name))
    self._disabled = disabled

## leaving example here to determine who calls property
# logging.Logger._disabled = logging.Logger.disabled
# logging.Logger.disabled = property(get_disabled, set_disabled)

if __name__ == '__main__':

    log2 = logging.getLogger('pynamodb.connection.base')
    log2.level = logging.DEBUG
    log3 = logging.getLogger('pynamodb.models')
    log3.level = logging.DEBUG

    log2.debug('log2')
    log3.debug('log3')
    log.debug('logging')

    #db_dump('all')
    #db_load('all')



