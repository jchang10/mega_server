
import datetime, jwt, os

from flask import current_app, g

from werkzeug import cached_property

from warrant import UserObj, dict_to_cognito
from warrant import Cognito as WarrantCognito

from app.extensions.cognito import cognito
from app.modules.auth import jwt_validator


class User(UserObj):    
    cognito = cognito

    # def __init__(self, username=None, name=None, id=None):
    #     self.id = id
    #     self.username = username
    #     self.name = name
    #     self.sub = id
    #     self.active = True

    def pyjwt_encode_auth_token(self, user_id):
        """Generates the auth token"""
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(
                    days=current_app.config.get('TOKEN_EXPIRATION_DAYS'),
                    seconds=current_app.config.get('TOKEN_EXPIRATION_SECONDS')
                ),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(
                payload,
                current_app.config.get('SECRET_KEY'),
                algorithm='HS256'
            )
        except Exception as e:
            return e


    @staticmethod
    def pyjwt_decode_auth_token(auth_token):
        """Decodes the auth token - :param auth_token: - :return: integer|string"""
        try:
            payload = jwt.decode(
                auth_token, current_app.config.get('SECRET_KEY'), verify=False)
            return payload
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'


    @staticmethod
    def get_claims(**kwargs):
        userpool_id = User.cognito.userpool_id
        aws_region = User.cognito.userpool_id.split('_')[0]
        client_id = User.cognito.client_id
        if kwargs.get('id_token'):
            token = kwargs.pop('id_token')
        else:
            token = kwargs.pop('access_token')
        return jwt_validator.get_claims(aws_region,
                                        userpool_id,
                                        token,
                                        client_id,
                                        #disable verify_at_hash which causes problems.
                                        #disable verify_signature which causes problems with federated login tokens.
                                        options={
                                            'verify_at_hash':False,
                                            'verify_signature':False,
                                            'verify_aud':False,
                                            'verify_iss':False,
                                        },
                                        **kwargs,
        )


    @classmethod
    def from_id_claims(cls, id_claims):
        claims = id_claims
        username = claims.get('cognito:username', 'NA')
        attribute_list = dict_to_cognito(claims)
        cognito_obj = User.cognito
        metadata = None
        attr_map = None
        user = cls(username,
                    attribute_list,
                    cognito_obj,
                    metadata,
                    attr_map)
        return user


    @classmethod
    def from_warrant_user(cls, warrant_user):
        username = warrant_user.username
        attribute_list = []
        cognito_obj = warrant_user._cognito
        metadata = warrant_user._metadata
        attr_map = warrant_user._attr_map
        user = cls(username,
                   attribute_list,
                   cognito_obj,
                   metadata,
                   attr_map)
        user._data = warrant_user._data
        return user
    

    @staticmethod
    def from_decode_auth_token(id_token, access_token):
        """Decode COGNITO auth token and return user based on given token
        :param id_token string
        :param access_token string
        :return User object|string """
        try:
            id_claims = User.get_claims(id_token=id_token)
            user = User.from_id_claims(id_claims)
            user.id_token = id_token
            user.id_claims = id_claims
            user.access_token = access_token
            if access_token:
                access_claims = User.get_claims(access_token=access_token)
                user.access_claims = access_claims
            #claims to user
            return user
        except Exception as e:
            pass
        return 'Invalid token. Please log in again.'

        
    @staticmethod
    def from_auth_tokens(auth_tokens):
        if ':' in auth_tokens:
            id_token, access_token = auth_tokens.split(':', 1)
            auth_user = User.from_decode_auth_token(id_token=id_token, access_token=access_token)
        else:
            id_token = auth_tokens
            access_token = None
            auth_user = User.from_decode_auth_token(id_token=id_token, access_token=access_token)

        # According to spec, id_token is not enough. access_token
        # must also be given for validation. However, we have
        # turned this check off later in the code, so this should
        # work.  With a cognito login, id_token is enough because
        # it does not have a at_hash claim to validate, which
        # requires the access_token.

        # auth_user = User.warrant_decode_auth_token(id_token=id_token, access_token=access_token)

        # The access_token does not seem to provide identifying
        # user information such as username userid, email, etc,
        # unfortunately. The id_token does provide user
        # information, however, it requires also providing the
        # access_token to verify its claims.

        # auth_user = User.warrant_decode_auth_token(access_token=access_token)

        if isinstance(auth_user, str):
            return auth_user

        # user = User.query.filter_by(id=resp).first()
        ''' # extra check retrieving user object.
        warrant_user = cognito.get_user(email=auth_user.email)
        if not warrant_user or not warrant_user.enabled:
            return response_object, code
        return f(warrant_user, *args, **kwargs)
        '''
        return auth_user

    @staticmethod
    def warrant(username=None):
        return WarrantCognito(User.cognito.userpool_id, User.cognito.client_id, username=username)

    @staticmethod
    def warrant_authenticate(username, password):
        wc = User.warrant(username)
        wc.authenticate(password=password)
        return wc

    @staticmethod
    def get_user(username=None, email=None):
        """ using admin_get_user() to get a full list of cognito user fields. """
        if username is None and email is None:
            raise ValueError('username or email must be given')
        wc = None
        if username:
            wc = User.warrant(username)
        if email:
            wc = User.warrant(email)
        u = wc.admin_get_user()
        return u

    @staticmethod
    def get_users():
        """ return list of users. limited set of fields compared to admin_get_user() above.
        specifically, no create_date field. """
        wc = User.warrant()
        return wc.get_users()
    
