from app.modules.users.models import User
from mytests.base import BaseTestCase

class TestUserModel(BaseTestCase):

    def test_add_user(self):
        user = self._add_test_user1()
        #self.assertTrue(user.id)
        self.assertEqual(user.username, 'jc-test1')
        self.assertEqual(user.email, 'jc-test1@jline.com')
        #self.assertTrue(user.password)
        self.assertTrue(user.enabled)
        self.assertTrue(user.user_create_date)
        #self.assertTrue(user.is_admin == False)
    '''
    def test_encode_auth_token(self):
        user = self.add_user('test@test.com', 'test user')
        auth_token = user.encode_auth_token(user.id)
        self.assertTrue(isinstance(auth_token, bytes))

    def test_decode_auth_token(self):
        user = self.add_user('test@test.com', 'test user')
        auth_token = user.encode_auth_token(user.id)
        self.assertTrue(isinstance(auth_token, bytes))
        self.assertEqual(User.decode_auth_token(auth_token), user.id)
    '''
    def test_add_user_duplicate_username(self):
        self.create_test_user('test', 'Test User', 'Test2test', email='test@test.com')
        with self.assertRaisesRegex(Exception, 'UsernameExistsException'):
            self.create_test_user('test', 'Test User 2', 'Test2test', email='test@test.com')
    '''
    def test_add_user_duplicate_email(self):
        add_user('justatest', 'test@test.com', 'test')
        duplicate_user = User(
            username='justatest2',
            email='test@test.com',
            password='test'
        )
        db.session.add(duplicate_user)
        self.assertRaises(IntegrityError, db.session.commit)

    def test_passwords_are_random(self):
        user_one = add_user('justatest', 'test@test.com', 'test')
        user_two = add_user('justatest2', 'test@test2.com', 'test')
        self.assertNotEqual(user_one.password, user_two.password)
    '''
