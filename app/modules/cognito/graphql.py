
import graphene
import graphene.types.datetime
from graphene_pynamodb import PynamoObjectType
from graphql import GraphQLError

from app.modules.auth.resources import current_user
from app.modules.users.models import User
from app.modules.graphql.schema import UserInterface

from warrant import UserObj


class CognitoUser(graphene.ObjectType):
    class Meta:
        interfaces = (UserInterface,)
    #user_create_date is a cognito created attribute
    create_date = graphene.types.datetime.DateTime(source='user_create_date')

    @classmethod
    def is_type_of(cls, value, context, info):
        """ allow cognito UserObj's to be a compatible type """
        """ See https://github.com/graphql-python/graphene/issues/699#issuecomment-378751225 """
        if isinstance(value, UserObj):
            return True
        return False


class CognitoQuery(graphene.AbstractType):
    admin_get_user = graphene.Field(CognitoUser,
                                    username = graphene.String()
    )
    
    def resolve_admin_get_user(self, info, **args):
        username = args.get('username')
        return User.get_user(username=username)
