from typing import List, ValuesView, Iterator
import boto3, logging, os
from botocore.exceptions import ClientError
import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from graphene_sqlalchemy.converter import convert_sqlalchemy_type, get_column_doc, is_column_nullable
from sqlalchemy_utils import UUIDType

from app.modules.faces_v2.models import FaceDb, PartDb
from app.modules.faces_v2.services import process_face_asset

log = logging.getLogger(__name__)
s3client = boto3.client('s3')


@convert_sqlalchemy_type.register(UUIDType)
def convert_column_to_string(type, column, registry=None):
    return graphene.String(description=get_column_doc(column),
                            required=not(is_column_nullable(column)))


class Part(SQLAlchemyObjectType):
    class Meta:
        model = PartDb
        interfaces = (graphene.Node,)

    url = graphene.String()
    s3_key = graphene.String()
    s3_bucket = graphene.String()
    version_id = graphene.String() # marked s3 version_id

    @classmethod
    def is_type_of(cls, result, info):
        """ This is required otherwise, get a type-mismatch error
            in MarksQuery.resolve_parts. """
        return True

    def resolve_url(self:PartDb, info, **args):
        """ http url """
        if args.get('presigned'):
            bucket = self._face.bucket
            s3_key = self.s3_key
            params = {'Bucket':bucket,
                'Key':s3_key,
            }
            if self._version_id:
                params['VersionId'] = self._version_id
            url = s3client.generate_presigned_url('get_object', Params=params) # default ExpiresIn=3600
        else:
            url = f'https://s3-us-west-2.amazonaws.com/{self._face.path}/{self.path}'
            if self._version_id:
                url = url+f'?versionId={self._version_id}'

        return url
        
    def resolve_s3_key(self:PartDb, info, **args):
        return self.s3_key

    def resolve_s3_bucket(self, info, **args):
        assert self.face, "Error. Associated parent face object missing."
        return self.face.bucket

    def resolve_version_id(self, info, **args):
        return self._version_id    
    

class Face(SQLAlchemyObjectType):
    class Meta:
        model = FaceDb
        exclude_fields = ('parts_map')
        interfaces = (graphene.Node,)

    parts = graphene.List(graphene.NonNull(Part),
                            gender = graphene.String(),
                            part = graphene.String())
    s3_bucket = graphene.String()

    def resolve_parts(self:FaceDb, info, **args):
        parts = self.parts
        return parts

    def resolve_s3_bucket(self, args, contxt, info) -> str:
        return self.bucket


class FacesQuery(graphene.AbstractType):
    faces_v2 = graphene.List(graphene.NonNull(Face), required=True)

    def resolve_faces_v2(self, info, **args) -> List[FaceDb]:
        return list(FaceDb.query.all())

class PartV2Connection(graphene.relay.Connection):
    class Meta:
        node = Part

class PartsQuery(graphene.AbstractType):
    parts_v2 = graphene.List(graphene.NonNull(Part))
    parts_v2_conn = SQLAlchemyConnectionField(PartV2Connection)

    def resolve_parts_v2(self, info, **args) -> ValuesView[PartDb]:
        faces:Iterator[FaceDb] = FaceDb.query.all()
        parts = { f'{face.prefix}{fpart.path}' : fpart 
                      for face in faces
                        for fpart in face.parts }
        return parts.values()


class ProcessS3FaceV2(graphene.ClientIDMutation):
    face = graphene.Field(Face)

    class Input:
        bucket = graphene.String()
        prefix = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **args) -> List[FaceDb]:
        face = process_face_asset(bucket_name = args.get('bucket'), 
                                  prefix=args.get('prefix'))
        return ProcessS3FaceV2(face=face)


class ProcessMutations(graphene.AbstractType):
    process_s3_face_v2 = ProcessS3FaceV2.Field()
