from botocore.exceptions import ClientError

from flask import g
from flask_restplus_patched import Namespace, Resource

from app.modules.auth.resources import authenticate
from app.modules.users.models import User


api = Namespace('cognito', description='Users description')


@api.route('/ping')
class PingPong(Resource):
    def get(self):
        return {
            'status': 'success',
            'message': 'pong!'
        }
    

@api.route('/admin_get_user/<username>')    
class AdminGetUser(Resource):
    @authenticate(admin=True)
    def get(self, username):
        """Get single user details"""
        response_object = {
            'status': 'fail',
            'message': 'User does not exist'
        }
        try:
            try:
                user = User.get_user(username=username)
            except ClientError as e:
                user = None
            if not user:
                return response_object, 404
            else:
                response_object = {
                    'status': 'success',
                    'data': {
                        'username': user.username,
                        'email': user.email,
                        'enabled': user.enabled,
                        'created_at': user.created_at
                    }
                }
                return response_object, 200
        except ValueError:
            return response_object, 404


@api.route('/get_user')
class GetUser(Resource):
    """Get single user details"""
    @authenticate()
    def get(self):
        response_object = {
            'status': 'fail',
            'message': 'User does not exist'
        }
        try:
            access_token = g.current_user.access_token
            warrant = User.warrant(g.current_user.username)
            warrant.access_token = access_token
            user = warrant.get_user()
            if not user:
                return response_object, 404
            else:
                response_object = {
                    'status': 'success',
                    'data': {
                      'username': user.username,
                      'email': user.email,
                      'is_admin': user.is_admin
                    }
                }
                return response_object, 200
        except ValueError:
            return response_object, 404


        
