# Testing setup stuff

from api.cognito import Cognito
from warrant import Cognito as WarrantCognito

# testpool1 - aws test userpool
userpool1_id='us-west-2_l8TqJksHs'
client1_id='69ger2tdv8c82kjcpnje6rqb5'
# testpool2 - local dev userpool
userpool2_id='us-west-2_7uh4yp56X'
client2_id='6a3ufap41ig5u15bufv0lolcr4'

cognito1 = Cognito(userpool1_id,client1_id)
cognito2 = Cognito(userpool2_id,client2_id)
warrant1 = WarrantCognito(userpool1_id,client1_id)
warrant2 = WarrantCognito(userpool2_id,client2_id)

'''
test_user1 = 'jc-test1@jline.com'
cognito.admin_create_user(test_user1,'Test1','Test2test')
warrant = cognito.warrant(username)
user = warrant.admin_get_user()
warrant.new_password_challenge('Test2test','Test2test')
cognito_username = user._metadata['username']
cognito.admin_add_user_to_group(cognito_username, 'TestUsers')
'''
test_user1 = 'jc-test1@jline.com'
test_user2 = 'jc-test2@jline.com'

