import graphene
from graphene.types.datetime import DateTime
from graphene_pynamodb import PynamoObjectType
from graphql import GraphQLError

from app.modules.auth.resources import current_user
from app.modules.graphql.schema import UserInterface
from app.modules.users.models import User


class UsersQuery(graphene.AbstractType):
    user = graphene.Field(UserInterface,
                          username = graphene.String(),
    )
    users = graphene.List(UserInterface)
        
    def resolve_user(self, info, **args):
        username = args.get('username')
        return User.get_user(username=username)

    def resolve_users(self, info, **args):
        return User.get_users()
