import os
import unittest

from flask import current_app
from flask_testing import TestCase

from app import create_app

class TestDevelopmentConfig(TestCase):
    def create_app(self):
        app = create_app('dev')
        return app

    def test_app_is_development(self):
        self.assertFalse(current_app is None)
        self.assertTrue(self.app.config['SECRET_KEY'])
        self.assertTrue(self.app.config['DEBUG'] is True)
        self.assertTrue(self.app.config['AWS_COGNITO_SIGNIN_URL'] ==
                        'https://localhost:5000/auth/cognito_signin')

        
class TestTestingConfig(TestCase):
    def create_app(self):
        app = create_app('test')
        return app

    def test_app_is_testing(self):
        self.assertFalse(current_app is None)
        self.assertTrue(self.app.config['SECRET_KEY'])
        self.assertTrue(self.app.config['DEBUG'])
        self.assertTrue(self.app.config['TESTING'])
        self.assertTrue(self.app.config['AWS_COGNITO_SIGNIN_URL'] in
                        ('https://3f1yfvft63.execute-api.us-west-2.amazonaws.com/test/auth/cognito_signin',
                         'https://api.withme.live/auth/cognito_signin'))


class TestProductionConfig(TestCase):
    def create_app(self):
        app = create_app('prod')
        return app

    def test_app_is_production(self):
        self.assertFalse(current_app is None)
        self.assertTrue(self.app.config['SECRET_KEY'])
        self.assertFalse(self.app.config['DEBUG'])
        self.assertFalse(self.app.config['TESTING'])
        #self.assertTrue(self.app.config['AWS_COGNITO_SIGNIN_URL'] == '')


if __name__ == '__main__':
    unittest.main()
