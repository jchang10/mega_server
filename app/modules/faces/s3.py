import boto3, logging
from typing import Dict, Iterator, List

log = logging.getLogger(__name__)

### MAIN FACE STORE BUCKET
BUCKET_NAME = 'withmelabs-faces'
BUCKET_PREFIX = 'FaceAssets/'
COMMON = '_common'

def get_bucket(bucket:str):
    """ returns the bucket resource """
    s3 = boto3.resource('s3')
    return s3.Bucket(bucket)

