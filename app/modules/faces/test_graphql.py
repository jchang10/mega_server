import json, os, types
from collections import OrderedDict
from graphene.test import Client
from app.modules.graphql import schema as myschema

client = Client(myschema)

def capital_case(x: str) -> str:
    return x.capitalize()

def test_capital_case():
    assert capital_case('semaphore') == 'Semaphore'

def test2_hello():
    result = client.execute('''query { hello }''')
    assert result == {
        'data': OrderedDict({'hello':'World '})
    }

def test_env():
    print(list(os.environ))

def test_faces_hairs():
  QUERY = """
    query {
      mark {
        faces {
          name
          parts {
            url
            part
            gender
          }
        }
        hairs {
          name
          parts {
            url
            filename
          }
        }
      }
    }
  """
  context = types.SimpleNamespace()
  result = client.execute(QUERY, context=context)
  assert result['data'], "Result missing!"
  assert result['data']['mark'], "Mark missing!"
  assert len(result['data']['mark']['faces']), "Faces missing!"
  assert len(result['data']['mark']['hairs']), "Hairs missing!"

def test_diffmarks():
  QUERY_MARKS = """
    query {
      marks {
        uuid createDate
      }
    }
  """
  context = types.SimpleNamespace()
  result = client.execute(QUERY_MARKS, context=context)
  assert result['data'], "Result missing"
  marks = result['data']['marks']
  assert len(marks)>=2, "Marks missing"
  newer_mark_id = marks[0]['uuid']
  older_mark_id = marks[1]['uuid']
  assert newer_mark_id and older_mark_id, "newer_mark_id or older_mark_id is not valid!"
  QUERY_DIFFMARKS = ('''
    query {'''
      f'''diffMarks(newMarkId:"{newer_mark_id}",
        oldMarkId:"{older_mark_id}"'''
      ''') {
        diffFaces {
          name
          parts {
            uuid url
          }
        }
        diffHairs {
          name
          parts {
            uuid url
          }
        }
        deletedParts {
          deleted url
        }
      }
    }
  ''')
  context = types.SimpleNamespace()
  result = client.execute(QUERY_DIFFMARKS, context=context)
  assert result['data'], "Result missing!"
  assert result['data']['diffMarks'], "DiffMarks Missing!"
  assert result['data']['diffMarks']['diffFaces'] is not None, "diffFaces missing!"
  assert result['data']['diffMarks']['diffHairs'] is not None, "diffHairs missing!"
  assert result['data']['diffMarks']['deletedParts'] is not None, "deletedParts missing!"
