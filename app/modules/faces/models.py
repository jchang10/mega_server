import boto3, datetime, logging, os, uuid
from pynamodb.attributes import UnicodeAttribute, BooleanAttribute, UTCDateTimeAttribute, NumberAttribute, MapAttribute
from pynamodb.indexes import GlobalSecondaryIndex, KeysOnlyProjection, AllProjection
import pynamodb.models
from typing import Dict, Iterator, List

import config

log = logging.getLogger(__name__)
s3client = boto3.client('s3')

class BaseMeta:
    # region = boto3.Session().region_name # otherwise, defaults to us-east-1. just requires AWS_PROFILE to be set
    region = 'us-west-2'
    # increase retry time to make things better
    base_backoff_ms = 5000
    
    def get_table_name(name):
        return config.config.get_project_path() + name

class BaseMixin:
    @classmethod
    def mycreate_table(cls, rcu=1, wcu=1):
        cls.create_table(read_capacity_units=rcu, write_capacity_units=wcu)

    @classmethod
    def delete_all(cls):
        for o in cls.scan():
            o.delete()

class VersionedMixin:
    createDate = UTCDateTimeAttribute(null=True)
    updateDate = UTCDateTimeAttribute(null=True)
    version = NumberAttribute(default=0)
    
    isnew = False # flag for get_or_create() method

    def set_attributes(self, **props) -> None:
        """ set a lot of attributes at once """
        self._set_attributes(**props) #type:ignore

    def update_versioning(self) -> None:
        """ update only version related attributes """
        self.version = self.version + 1
        self.updateDate = datetime.datetime.utcnow()
        
    def save(self, *args, **kwargs):
        if not self.isnew:
            self.update_versioning()
        result = super().save(*args, **kwargs)
        self.isnew = False
        return result

class FacePartsFaceIndex(GlobalSecondaryIndex):
    class Meta:
        read_capacity_units = 1
        write_capacity_units = 1
        # All attributes are projected
        projection = AllProjection()

    faceId = UnicodeAttribute(hash_key=True)
    path = UnicodeAttribute(range_key=True)

class FacePartsPartKeyIndex(GlobalSecondaryIndex):
    class Meta:
        read_capacity_units = 1
        write_capacity_units = 1
        # All attributes are projected
        projection = AllProjection()

    partKey = UnicodeAttribute(hash_key=True)

class FacePartsDb(VersionedMixin, BaseMixin, pynamodb.models.Model):
    class Meta(BaseMeta):
        table_name = BaseMeta.get_table_name('FaceParts')
    uuid = UnicodeAttribute(hash_key=True)
    faceId = UnicodeAttribute()
    path = UnicodeAttribute()
    owner = UnicodeAttribute(null=True)
    size = NumberAttribute()
    part = UnicodeAttribute(null=True)
    gender = UnicodeAttribute(null=True)
    deleted = BooleanAttribute(null=True)
    etag = UnicodeAttribute(null=True)
    partKey = UnicodeAttribute(null=True)

    faceIndex = FacePartsFaceIndex()
    partKeyIndex = FacePartsPartKeyIndex()

    _face:'FaceAssetsDb' = None
    _mark:'FaceMarksDb' = None

    @classmethod
    def get_or_create(cls, face:'FaceAssetsDb', path:str) -> 'FacePartsDb':
        parts = list( cls.faceIndex.query(
            face.uuid,
            range_key_condition = cls.path == path,
            limit = 1
        ))
        if len(parts): #exists
            part = parts[0]
            part.isnew = False
        else: #new
            part = cls(
                str(uuid.uuid4()),
                faceId = face.uuid,
                path = path,
                createDate = datetime.datetime.utcnow()
            )
            part.isnew = True
        # part._face = face
        return part

    @property
    def s3_key(self) -> str:
        """ Return the s3 key for this part. """
        assert self.face, "Face does not exist!"
        s3_key = f'{self.face.prefix}{self.path}'
        return s3_key

    def get_url(self, presigned=False, versionId=None) -> str:
        ''' construct the URL for this part. '''
        if presigned:
            bucket = self.face.bucket
            s3_key = self.s3_key
            params = {
                'Bucket':bucket,
                'Key':s3_key,
            }
            if versionId:
                params['VersionId'] = versionId
            url = s3client.generate_presigned_url('get_object', Params=params) # default ExpiresIn=3600
        else:
            url = f'https://s3-us-west-2.amazonaws.com/{self.face.path}{self.path}'
            if versionId:
                url = url+f'?versionId={versionId}'
        return url

    def set_mark(self, mark:'FaceMarksDb'):
        assert mark.typeKey == FaceMarksDb.PART_TYPE, 'Only FaceMarksDb "part" types can be assigned.'
        self._mark = mark
        mark._face_part = self
        return self

    @property
    def face(self) -> 'FaceAssetsDb':
        if self._face == None:
            self._face = FaceAssetsDb.get(self.faceId)
            assert self._face, "Part.faceId was not found!"
        return self._face


class FacePathIndex(GlobalSecondaryIndex):
    class Meta:
        # index_name is optional, but can be provided to override the default name
        #index_name = 'foo-index'
        read_capacity_units = 1
        write_capacity_units = 1
        # instead of AllProjection, let's experiment with KeysOnlyProjection.
        # probably shouldnt here because FaceAssets are fairly lightweight and not
        # much copy overhead exists.
        projection = KeysOnlyProjection()

    # This attribute is the hash key for the index
    # Note that this attribute must also exist
    # in the model
    path = UnicodeAttribute(hash_key=True)

class FaceAssetsDb(VersionedMixin, BaseMixin, pynamodb.models.Model):
    COMMON = '_common' # ignore face with this name. used in scans/queries

    class Meta(BaseMeta):
        table_name = BaseMeta.get_table_name('FaceAssets')
    uuid = UnicodeAttribute(hash_key=True) # faceId
    name = UnicodeAttribute()
    path = UnicodeAttribute()
    owner = UnicodeAttribute(null=True)

    facePathIndex = FacePathIndex()
    _parts:List[FacePartsDb] = None
    _bucket:str = None
    _prefix:str = None
    _mark:'FaceMarksDb' = None

    @classmethod
    def get_or_create(cls, path:str) -> 'FaceAssetsDb':
        faces = list(cls.facePathIndex.query(path, limit=1))
        if len(faces): #exists
            face = faces[0]
            # because we have a KeysOnlyProjection, we need to retrieve the entire object
            face = cls.get(face.uuid)
            face.isnew = False
        else: #new
            face = cls(
                str(uuid.uuid4()),
                path = path,
                createDate = datetime.datetime.utcnow()
            )
            face.isnew = True
        return face

    @property
    def bucket(self) -> str:
        if not self._bucket:
            (self._bucket, self._prefix) = self.path.split('/', 1)
        return self._bucket

    @property
    def prefix(self) -> str:
        if not self._prefix:
            (self._bucket, self._prefix) = self.path.split('/', 1)
        return self._prefix

    @property
    def face_name(self) -> str:
        return os.path.basename(self.path[:-1])
    
    @property
    def parts(self) -> List[FacePartsDb]:
        """ return all parts """
        if self._parts == None:
            parts = list(self.query_parts())
            self._parts = parts
        return self._parts

    @parts.setter
    def parts(self, parts:List[FacePartsDb]) -> None:
        self._parts = parts

    def query_parts(self, deleted=False) -> Iterator:
        """ example query with filter_condition """
        if deleted:
            condition = None
        else:
            condition = FacePartsDb.deleted.does_not_exist() | (FacePartsDb.deleted == False) #type:ignore #thinks deleted.does_not_exist() does not exist
        parts = FacePartsDb.faceIndex.query(self.uuid, condition)
        parts = map(lambda part: setattr(part, '_face', self) or part, parts) #type:ignore #setattr does not return value
        return parts

    def query_parts_map(self: 'FaceAssetsDb', deleted=False) -> Dict[str, FacePartsDb]:
        """ Return a map of FacePartsDb keyed by the part key.
            If deleted is True, include deleted parts, too.
        """
        parts_map:Dict = {}
        parts = self.query_parts(deleted=deleted)
        for part in parts:
            parts_map[part.partKey] = part
        return parts_map

    @classmethod
    def filter_parts(cls, parts:List['FacePartsDb'], gender=None, part=None) -> List['FacePartsDb']:
        if gender:
            parts = [p for p in parts if p.gender == gender]
        if part:
            parts = [p for p in parts if p.part == part]
        return parts
    
    def delete(self, cascade=True):
        """ delete the face_asset. cascade the face_parts if True. """
        if cascade:
            with FacePartsDb.batch_write() as batch:
                for part in self.query_parts(deleted=True):
                    part.delete()
        return super().delete()

    def set_mark(self, mark:'FaceMarksDb'):
        assert mark.typeKey == FaceMarksDb.MARK_TYPE, 'Only FaceMarksDb "mark" types can be assigned.'
        self._mark = mark
        mark._face = self
        return self


class MarkTypeIndex(GlobalSecondaryIndex):
    class Meta:
        # index_name is optional, but can be provided to override the default name
        #index_name = 'foo-index'
        read_capacity_units = 1
        write_capacity_units = 1
        # instead of AllProjection, let's experiment with KeysOnlyProjection.
        # probably shouldnt here because FaceAssets are fairly lightweight and not
        # much copy overhead exists.
        projection = AllProjection()
    # This attribute is the hash key for the index
    # Note that this attribute must also exist
    # in the model
    typeKey = UnicodeAttribute(hash_key=True)
    createDate = UTCDateTimeAttribute(range_key=True)

class MarkVersionIdIndex(GlobalSecondaryIndex):
    class Meta:
        # index_name is optional, but can be provided to override the default name
        #index_name = 'foo-index'
        read_capacity_units = 1
        write_capacity_units = 1
        # AllProjection(), KeysOnlyProjection(), or IncludeProjection(non_attr_keys)
        projection = KeysOnlyProjection()
    # This attribute is the hash key for the index
    # Note that this attribute must also exist
    # in the model
    versionIdKey = UnicodeAttribute(hash_key=True)
    partKey = UnicodeAttribute(range_key=True)

class MarkPartsIndex(GlobalSecondaryIndex):
    """ Index all parts for given markId """
    class Meta:
        read_capacity_units = 1
        write_capacity_units = 1
        projection = AllProjection()
    markId = UnicodeAttribute(hash_key=True)

class FaceMarksDb(VersionedMixin, BaseMixin, pynamodb.models.Model):
    class Meta(BaseMeta):
        table_name = BaseMeta.get_table_name('FaceMarks')

    # the different type key values
    MARK_TYPE = 'mark'
    PART_TYPE = 'part'

    uuid = UnicodeAttribute(hash_key=True)
    typeKey = UnicodeAttribute()
    
    ### MARK_TYPE attributes
    bucket = UnicodeAttribute(null=True)
    prefix = UnicodeAttribute(null=True)
    partVersions:MapAttribute = MapAttribute(null=True, default={})
    is_valid = BooleanAttribute(null=True)
    is_publish = BooleanAttribute(null=True)

    ### PART_TYPE attributes
    # if markId is None, then this is a Mark type.
    # if markId is a value, then this is a Part type that is part of the Mark.
    markId = UnicodeAttribute(null=True)
    # partId = UnicodeAttribute(null=True)
    partKey = UnicodeAttribute(null=True)
    versionId = UnicodeAttribute(null=True)
    createDate = UTCDateTimeAttribute(null=True)

    markTypeIndex = MarkTypeIndex()
    markPartsIndex = MarkPartsIndex()
    markVersionIdIndex = MarkVersionIdIndex()

    _face:FaceAssetsDb = None # backref to FaceAssetsDb
    _face_part:FacePartsDb = None # backref to FacePartsDb

    @classmethod
    def query_last(cls, prev:int = 1) -> 'FaceMarksDb':
        """ query for the last {prev} marks. prev=1 is the last. prev=2 is next last. etc. """
        # should return the latest at index 0
        marks = list(FaceMarksDb.markTypeIndex.query(FaceMarksDb.MARK_TYPE, limit=prev, scan_index_forward=False))
        mark = marks[0] if marks else None
        return mark

    def delete(self, cascade=True):
        """ Delete the FaceMarksDb and all of its parts. """
        marks = FaceMarksDb.markPartsIndex.query(self.uuid)
        if cascade:
            with FaceMarksDb.batch_write() as batch:
                for mark in marks:
                    mark.delete()
        return super().delete()


from promise import Promise
from promise.dataloader import DataLoader

class FacePartsLoader(DataLoader):
    def batch_load_fn(self, keys):
        # Here we return a promise that will result on the
        # corresponding user for each key in keys
        return Promise.resolve([FacePartsDb.get(key) for key in keys])    

class FaceAssetsLoader(DataLoader):
    def batch_load_fn(self, keys):
        # Here we return a promise that will result on the
        # corresponding user for each key in keys
        return Promise.resolve([FaceAssetsDb.get(key) for key in keys])


def delete_all():
    FaceAssetsDb.delete_all()
    FacePartsDb.delete_all()
    FaceMarksDb.delete_all()

def test1():
    mark = FaceMarksDb.get_or_create()
    mark.save()

def test2():
    mark = FaceMarksDb.get_or_create('eed10c0c-766e-4ee1-a173-65c9dd27c162')
    mark.partVersions['testme'] = 'testval'
    mark.save()

def migrate_y2018_1010():
    for face in FaceAssetsDb.scan():
        for part in face.query_parts(deleted=True):
            part.s3key = None
            part.save()

if __name__ == '__main__':
    # handler = logging.StreamHandler()
    # handler.setLevel(logging.DEBUG)
    # log.addHandler(handler)
    # log.setLevel(logging.DEBUG)

    test2()

