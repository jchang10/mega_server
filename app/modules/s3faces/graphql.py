from typing import List, ValuesView, Iterator
import boto3, os, logging, six
from botocore.exceptions import ClientError
import graphene, graphene.types, graphene.relay
from graphene_pynamodb import PynamoObjectType, PynamoConnectionField

from app.modules.faces.models import FaceAssetsDb, FacePartsDb, FaceMarksDb
from app.modules.faces.graphql import Faces, Parts
from app.modules.faces.s3 import BUCKET_NAME, BUCKET_PREFIX
from app.modules.s3faces.services import fetch_s3objects, process_face_asset, process_face_container, join_s3_part_objects, copy_s3_objects

log = logging.getLogger(__name__)


class S3ObjectAbstractType(graphene.AbstractType):
    key = graphene.String()
    size = graphene.Int()
    last_modified = graphene.types.datetime.DateTime()


class S3ObjectType(S3ObjectAbstractType, graphene.ObjectType):
    pass


class S3PartObjectType(S3ObjectAbstractType, graphene.ObjectType):
    version = graphene.Int()
    status = graphene.String() # can also be undef if the part's create_date and update_dates are both None
    part = graphene.Field(Parts)

#region failed to implement pagination with S3


class S3ObjectTypeConnection(graphene.relay.Connection):
    class Meta:
        node = S3ObjectType


class S3ListObjectsConnectionField(graphene.relay.ConnectionField):

    def __init__(self, type, *args, **kwargs):
        super(S3ListObjectsConnectionField, self).__init__(
            type, *args, **kargs
        )

""" def get_paginator(bucket:str, prefix:str):
    s3client = boto3.client('s3')
    result = s3client.list_objects_v2(Bucket=bucket, Prefix=prefix, Maxkeys=5)
    return result

def connection_from_cursor_paginated(connection_type, edge_type, pageinfo_type, **kwargs):
    full_args = dict(args, **kwargs)
    result = get_paginator(**full_args)
    page = result['Contents']

    edges = []
    for item in page:
        # edge = edge_type(node=item, cursor=paginator.cursor(item))
        edge = edge_type(node=item, cursor=item.Key)
        edges.append(edge)

    return connection_type(
        edges=edges,
        page_info=pageinfo_type(
            start_cursor=result['NextContinuationToken'],
            end_cursor=None,
            has_previous_page=None,
            has_next_page= True if 'NextContinuationToken' in result else False
        )
    )
'''
class CursorPaginatedConnection(graphene.relay.Connection):
    @classmethod
    def from_list(cls, queryset, args, context, info):
        connection = connection_from_cursor_paginated(queryset, args, connection_type=cls, edge_type=cls.edge_type, pageinfo_type=graphene.relay.PageInfo)
        connection.set_connection_data(queryset)
        return connection
'''
class CursorPaginatedConnectionField(graphene.relay.ConnectionField):
    def __init__(self, *args, **kwargs):
        kwargs['connection_type'] = kwargs.pop('connection_type', CursorPaginatedConnection)
        super(CursorPaginatedConnectionField, self).__init__(*args, **kwargs)
 """
#endregion


class S3FaceQuery(graphene.AbstractType):
    # s3_objects = graphene.List(
    #     graphene.NonNull(S3ObjectType),
    #         bucket = graphene.String(),
    #         prefix = graphene.String())
    s3_objects = graphene.relay.ConnectionField(S3ObjectTypeConnection,
        bucket = graphene.String(),
        prefix = graphene.String()
    )
    s3_part_objects = graphene.List(
        graphene.NonNull(S3PartObjectType),
        bucket = graphene.String(),
        prefix = graphene.String(),
    )

    def resolve_s3_objects(self, info, bucket, prefix) -> List:
        """ return the straight s3objects"""
        s3objects = fetch_s3objects(bucket, prefix)
        return list(s3objects.values())
        # s3 = boto3.client('s3')
        # paginator = s3.get_paginator('list_objects_v2')
        # pages = paginator.paginate(Bucket='withmelabs-faces', Prefix='FaceAssets', PaginationConfig={'MaxItems':100, 'PageSize':10})
        # for i, page in enumerate(iter(pages)):
        #     print(i,'**',page)
        # return []

    def resolve_s3_part_objects(self, info, **args):
        if not 'prefix' in args:
            args['prefix'] = BUCKET_PREFIX
        s3partobjs = join_s3_part_objects(**args)
        return s3partobjs.values()
        

class CopyS3Objects(graphene.ClientIDMutation):
    errors = graphene.List(graphene.String)

    class Input:
        bucket = graphene.String()
        prefix = graphene.String()
        paths = graphene.List(graphene.String)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **args):
        errors = copy_s3_objects(**args)
        return CopyS3Objects(errors=errors)


class ProcessS3Face(graphene.ClientIDMutation):
    face = graphene.Field(Faces)

    class Input:
        bucket = graphene.String()
        prefix = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **args) -> List[FaceAssetsDb]:
        face = process_face_asset(bucket_name = args.get('bucket'), 
                                  prefix=args.get('prefix'))
        return ProcessS3Face(face=face)


class ProcessS3Faces(graphene.ClientIDMutation):
    faces = graphene.List(graphene.NonNull(Faces))

    class Input:
        bucket = graphene.String()
        prefix = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **args) -> List[FaceAssetsDb]:
        faces = process_face_container(bucket_name = args.get('bucket'), 
                                        prefix=args.get('prefix'))
        return ProcessS3Faces(faces=faces)


class S3FaceMutations(graphene.AbstractType):
    copyS3Objects = CopyS3Objects.Field()
    processS3Face = ProcessS3Face.Field()
    processS3Faces = ProcessS3Faces.Field()






