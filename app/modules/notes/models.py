import boto3, os
from pynamodb.attributes import UnicodeAttribute, BooleanAttribute, UTCDateTimeAttribute, NumberAttribute
import pynamodb.models

import config

class BasePynamo:
#    region = boto3.Session().region_name # otherwise, defaults to us-east-1. just requires AWS_PROFILE to be set
    region = 'us-west-2' #force set
    
    def get_table_name(name):
        return config.config.get_project_path() + name

class NotesDb(pynamodb.models.Model):
    class Meta(BasePynamo):
        table_name = BasePynamo.get_table_name('notes')

    # Swagger cannot handle nulls, so default values must be provided here.
    # otherwise, the UI will error out.
    user_id = UnicodeAttribute(hash_key=True, default='null') #default='null' required for swagger
    note1 = UnicodeAttribute(null=True, default='null') #default='null' required for swagger
    note2 = UnicodeAttribute(null=True, default='null') #default='null' required for swagger
    note3 = UnicodeAttribute(null=True, default='null') #default='null' required for swagger

    @classmethod
    def mycreate_table(cls):
        cls.create_table(read_capacity_units=1, write_capacity_units=1)



import uuid
from app.extensions import db
from sqlalchemy_utils import UUIDType

class Notes2Db(db.Model):
    __tablename__ = 'notes2'
    uuid = db.Column(UUIDType(binary=False), primary_key=True)
    note1 = db.Column(db.String(100))
    note2 = db.Column(db.String(100))
    note3 = db.Column(db.String(100))
    

from app import create_app
def test_write():
    app = create_app()
    with app.app_context():
        note = Notes2Db(uuid=uuid.uuid4(), note1='testme1',note2='testme2',note3='testme3')
        db.session.add(note)
        db.session.commit()

def test_read():
    app = create_app()
    with app.app_context():
        notes = Notes2Db.query.all()
        for note in notes:
            print('note:'+str(note))

import flask_migrate
import flask_migrate.cli
def test_upgrade():
    app = create_app()
    with app.app_context():
        flask_migrate.cli._upgrade()

import requests
def test1():
    data = requests.get('https://cognito-idp.us-west-2.amazonaws.com', timeout=10)
    print('data:'+str(data.text))