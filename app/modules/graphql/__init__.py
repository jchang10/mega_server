from functools import wraps
from flask import g, request
from flask_graphql import GraphQLView
import graphene

from app.modules.auth.resources import authenticate


class HelloQuery(graphene.AbstractType):
    hello = graphene.String(description='Hello',
                            name=graphene.String()
    )

    def resolve_hello(self, info, **args):
        return 'World ' + args.get('name', '')


import app.modules.users.graphql
import app.modules.cognito.graphql
import app.modules.notes.graphql
import app.modules.faces.graphql
import app.modules.s3faces.graphql
import app.testme.sqlalchemy.graphql
import app.modules.faces_v2.graphql

class Query(
    app.modules.notes.graphql.NotesQuery,
    app.modules.cognito.graphql.CognitoQuery,
    app.modules.users.graphql.UsersQuery,
    app.modules.faces.graphql.FacesQuery,
    app.modules.faces.graphql.PartsQuery,
    app.modules.faces.graphql.MarksQuery,
    app.modules.s3faces.graphql.S3FaceQuery,
    app.testme.sqlalchemy.graphql.Query,
    app.modules.faces_v2.graphql.FacesQuery,
    app.modules.faces_v2.graphql.PartsQuery,
    HelloQuery,
    graphene.ObjectType
    ):
        pass

class Mutation(
    app.modules.notes.graphql.NotesMutation,
    app.modules.faces.graphql.FaceMutations,
    app.modules.faces.graphql.MarkMutations,
    app.modules.s3faces.graphql.S3FaceMutations,
    app.modules.faces_v2.graphql.ProcessMutations,
    graphene.ObjectType
    ):
        pass


schema = graphene.Schema(query=Query, mutation=Mutation)


# def hack_fix(func):
#     @wraps(func)
#     def wrapper(*args, **kwargs):
#         import ipdb;ipdb.set_trace()
#         if args.get('query', False):
#             test='test'        
#         return func(*args, **kwargs)
#     return wrapper


def init_app(app):
    view_func = GraphQLView.as_view('graphql',
                                    schema=schema,
                                    graphiql=bool(app.config.get("DEBUG", False)),
                )

    # view_func=hack_fix(view_func)
    view_func=authenticate(admin=False)(view_func)

    app.add_url_rule('/graphql', view_func=view_func)
