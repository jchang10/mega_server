import flask_marshmallow
from flask_marshmallow import base_fields

class BasicAuthResponseSchema(flask_marshmallow.Schema):
    status = base_fields.Str(required=True)
    message = base_fields.Str(required=True)
    

class LoginResponseSchema(BasicAuthResponseSchema):
    auth_token = base_fields.Str()


from app.modules.users.schemas import UserSchema    
class StatusResponseSchema(BasicAuthResponseSchema):
    user = base_fields.Nested(UserSchema)
    id_claims = base_fields.Dict()
    access_claims = base_fields.Dict()
