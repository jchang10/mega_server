from typing import List, ValuesView, Iterator, Dict
import boto3, logging, os
from botocore.exceptions import ClientError
import graphene
from graphene_pynamodb import PynamoObjectType, PynamoConnectionField
from graphql import GraphQLError

from app.modules.faces.models import FaceAssetsDb, FacePartsDb, FaceMarksDb
from app.modules.faces.s3 import BUCKET_NAME, BUCKET_PREFIX
from app.modules.faces.services import admin_purge_unused_versions, add_mark2, validate_mark, publish_mark, \
    join_parts_marks, join_hair_parts_marks, diff_marks

log = logging.getLogger(__name__)
s3client = boto3.client('s3')


class Parts(PynamoObjectType):
    class Meta:
        model = FacePartsDb
        interfaces = (graphene.Node,)
        description = 'Individual file asset'

    url = graphene.String(presigned=graphene.Boolean(), description='URL to file asset')
    s3_key = graphene.String()
    s3_bucket = graphene.String()
    versionId = graphene.String(  # marked s3 version_id
        description='The S3 version if the associated FaceMarksDb has been assigned')
    filename = graphene.String()

    @classmethod
    def is_type_of(cls, result, info):
        """ This is required otherwise, get a type-mismatch error
            in MarksQuery.resolve_parts. """
        return True

    def resolve_url(self:FacePartsDb, info, **args):
        presigned = args.get('presigned')
        versionId = self._mark.versionId if self._mark and self._mark.versionId else None
        return self.get_url(presigned=presigned, versionId=versionId)

    def resolve_s3_key(self:FacePartsDb, info, **args):
        """ s3 key. the s3 bucket is returned by FacesType. """
        return self.s3_key

    def resolve_s3_bucket(self:FacePartsDb, info, **args):
        assert self.face, "Error. Associated parent face object missing."
        return self.face.bucket

    def resolve_versionId(self:FacePartsDb, info, **args):
        assert self._mark, 'FaceMarksDb has not been associated with this part yet.'
        return self._mark.versionId

    def resolve_filename(self:FacePartsDb, info, **args):
        return os.path.basename(self.path)


class Faces(PynamoObjectType):
    class Meta:
        model = FaceAssetsDb
        interfaces = (graphene.Node,)
        description = 'A grouping of Parts'

    parts = graphene.List(graphene.NonNull(Parts),
                            gender = graphene.String(),
                            part = graphene.String(),
                            description='List of Parts for this FaceType')
    # diff_parts = graphene.List(Parts,
    #     description='List of only the parts that are new or different')
    s3_bucket = graphene.String()

    def resolve_parts(self:FaceAssetsDb, info, **args):
        if self._mark:
            """ return parts with their marks. """
            parts1:Dict[str, FacePartsDb] = join_parts_marks(self._mark, face=self)
            myparts = self.filter_parts(
                        list(parts1.values()),
                        gender = args.get('gender'),
                        part = args.get('part')
            )
            return myparts
        else:
            """ no mark, so just return parts """
            parts2 = self.filter_parts(
                        self.parts,
                        gender = args.get('gender'),
                        part = args.get('part')
            )
            return parts2

    # def resolve_diff_parts(self:FaceAssetsDb, info, **args):
    #     assert hasattr(info.context, 'diff_parts'), 'diff_parts missing!'
    #     parts = [
    #         part for part in info.context.diff_parts.values()
    #             if part.face.uuid == self.uuid
    #     ]
    #     return parts

    def resolve_s3_bucket(self:FaceAssetsDb, args, contxt, info) -> str:
        return self.bucket
        

class FacesQuery(graphene.AbstractType):
    faces = graphene.List(graphene.NonNull(Faces), required=True)
    face = graphene.Field(graphene.NonNull(Faces),
                        face_id = graphene.ID())
    faces_conn = PynamoConnectionField(Faces)

    def resolve_faces(self, info, **args) -> List[FaceAssetsDb]:
        return sorted(FaceAssetsDb.scan(), key=lambda face: face.name)

    def resolve_face(self, info, **args) -> FaceAssetsDb:
        face_id = args.get('face_id')
        assert face_id, 'face_id not given'
        face = FaceAssetsDb.get(face_id)
        return face

    def resolve_faces_conn(self, info, **args) -> List[FaceAssetsDb]:
        return sorted(FaceAssetsDb.scan(), key=lambda face: face.name)


class PartsQuery(graphene.AbstractType):
    parts = graphene.List(graphene.NonNull(Parts))
    parts_conn = PynamoConnectionField(Parts)

    def resolve_parts(self, info, **args) -> List[FacePartsDb]:
        parts = [ part
            for face in FaceAssetsDb.scan()
                for part in face.parts
        ]
        return parts

    def resolve_parts_conn(self, info, **args) -> List[FacePartsDb]:
        return PartsQuery.resolve_parts(self, info, **args)
        
class DeleteFace(graphene.ClientIDMutation):

    face = graphene.Field(Faces)
    class Input:
        face_id = graphene.ID()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **args):
        face = FaceAssetsDb.get(args.get('face_id'))
        if face:
            face.delete(cascade=True)
        return DeleteFace(face=face)

class UpdateFace(graphene.ClientIDMutation):
    face = graphene.Field(Faces)

    class Input:
        uuid = graphene.ID(required=True)
        name = graphene.String()
        path = graphene.String()
        owner = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **args):
        face_input = args.get('face_input')
        face = FaceAssetsDb.get(args.get('uuid'))
        if args.get('name'): face.name = args.get('name')
        if args.get('path'): face.name = args.get('path')
        if args.get('owner'): face.name = args.get('owner')
        face.save()
        return UpdateFace(face=face)


class FaceMutations(graphene.AbstractType):
    delete_Face = DeleteFace.Field()
    update_Face = UpdateFace.Field()

""" 
### an early version that subclasses Parts
class Hairs(Parts):
    class Meta:
        model = FacePartsDb
        interfaces = (graphene.Node,)
        description = 'Hair file asset'
 """
class Hairs(graphene.ObjectType):
    name = graphene.NonNull(graphene.String)
    parts = graphene.List(graphene.NonNull(Parts))


class Marks(PynamoObjectType):
    class Meta:
        model = FaceMarksDb
        interfaces = (graphene.Node,)
        description = 'Marked versions of face assets'

    face = graphene.Field(graphene.NonNull(Faces),
        name = graphene.String(),
        description='Return the face with the given name.')
    faces = graphene.List(graphene.NonNull(Faces),
        description='List of all the faces in this mark.')
    hairs = graphene.List(graphene.NonNull(Hairs),
        description='Return the hairs.')
    # notneeded
    # parts = graphene.List(graphene.NonNull(Parts),
    #     description='List of all the parts in this mark.')

    def resolve_face(self:FaceMarksDb, info, **args) -> FaceAssetsDb:
        faces = [face for face in FaceAssetsDb.scan(FaceAssetsDb.name == args.get('name'))]
        assert len(faces), "Face not found in DB!"
        face = faces[0]
        return face

    def resolve_faces(self:FaceMarksDb, info, **args) -> List[FaceAssetsDb]:
        faces = [ face.set_mark(self) for face in FaceAssetsDb.scan() ]
        return sorted(faces, key=lambda face: face.name)

    # notneeded
    # def resolve_parts(self:FaceMarksDb, info, **args) -> List[FacePartsDb]:
    #     """ Return list of all FacePartsDb with associated FaceMarksDb """
    #     parts = [ part
    #         for face in FaceAssetsDb.scan()
    #             for part in join_parts_marks(self, face=face).values()
    #     ]
    #     return parts

    def resolve_hairs(self:FaceMarksDb, info, **args) -> List[Hairs]:
        faces = [face for face in FaceAssetsDb.scan(FaceAssetsDb.name == FaceAssetsDb.COMMON)]
        assert len(faces), "Hairs not found in DB!"
        face = faces[0]
        parts = join_hair_parts_marks(self, face)
        parts2 = {
            key : Hairs(name=key, parts=parts)
                for key, parts in parts.items()
        }
        return sorted(parts2.values(), key=lambda hair: hair.name)


class DiffMarks(PynamoObjectType):
    class Meta:
        model = FaceMarksDb
        interfaces = (graphene.Node,)
        description = 'Diff FaceMarks'
    
    diff_faces = graphene.List(Faces,
        description='List of new or modified faces.')
    deleted_faces = graphene.List(Faces,
        description='List of deleted face parts.')
    diff_hairs = graphene.List(Hairs,
        description='List of new or modified hairs.')
    deleted_hairs = graphene.List(Hairs,
        description='List of deleted hair parts.')
    diff_parts = graphene.List(Parts,
        description='List of only the parts that are new or different.')
    deleted_parts = graphene.List(Parts,
        description='List of parts assumed to be deleted.')

    def resolve_diff_parts(self:FaceMarksDb, info, **args) -> ValuesView[FacePartsDb]:
        """ Return list of all parts that are new or different. """
        if hasattr(info.context, 'diff_parts'):
            return info.context.diff_parts.values()
        else:
            return None

    def resolve_deleted_parts(self:FaceMarksDb, info, **args) -> ValuesView[FacePartsDb]:
        """ Return list of parts that have been deleted. """
        if hasattr(info.context, 'deleted_parts'):
            return info.context.deleted_parts.values()
        else:
            return None

    def _resolve_diff_deleted_faces(parts: Dict[str, FacePartsDb]) -> List[FaceAssetsDb]:
        """ Helper for resolve_diff_faces and resolve_deleted_faces """
        faces: Dict = {}
        for part in parts.values():
            if not part.face.name in (FaceAssetsDb.COMMON):
                face = part.face
                uuid = face.uuid
                if uuid not in faces:
                    faces[uuid] = face
                    face.parts = [part]
                else:
                    face.parts.append(part)
        return sorted(faces.values(), key=lambda face: face.name)

    def resolve_diff_faces(self: FaceMarksDb, info, **args) -> List[FaceAssetsDb]:
        assert hasattr(info.context, 'diff_parts'), 'info.context.diff_parts missing!'
        return DiffMarks._resolve_diff_deleted_faces(info.context.diff_parts)

    def resolve_deleted_faces(self: FaceMarksDb, info, **args) -> List[FaceAssetsDb]:
        assert hasattr(info.context, 'deleted_parts'), 'info.context.deleted_parts missing!'
        return DiffMarks._resolve_diff_deleted_faces(info.context.deleted_parts)

    def _resolve_diff_deleted_hairs(parts:Dict[str, FacePartsDb]) -> List[Hairs]:
        hairs:Dict = {}
        for key, part in parts.items():
            if part.part == 'hair':
                name = os.path.splitext(key)[0]
                name = os.path.basename(name)
                if name not in hairs:
                    hairs[name] = Hairs(name=name, parts=[part])
                else:
                    hairs[name].parts.append(part)
        return sorted(hairs.values(), key=lambda hair:hair.name)

    def resolve_diff_hairs(self: FaceMarksDb, info, **args) -> List[Hairs]:
        assert hasattr(info.context, 'diff_parts'), 'info.context.diff_parts missing!'
        return DiffMarks._resolve_diff_deleted_hairs(info.context.diff_parts)

    def resolve_deleted_hairs(self: FaceMarksDb, info, **args) -> List[Hairs]:
        assert hasattr(info.context, 'deleted_parts'), 'info.context.deleted_parts missing!'
        return DiffMarks._resolve_diff_deleted_hairs(info.context.deleted_parts)


class MarksQuery(graphene.AbstractType):
    marks = graphene.List(graphene.NonNull(Marks),
        is_valid = graphene.Boolean(),
        is_publish = graphene.Boolean(),
        description = ('''Return a list of Marks. By default, return all marks.'''
            ''' '''
            '''If arg is_valid given, then filter by the given is_valid value (true or false).'''
            '''If arg is_publish given, then filter by same.'''
            '''if both is_valid and is_publish given, then both conditions must be true. ''')
    )
    mark = graphene.Field(Marks,
        mark_id = graphene.ID(),
        prev = graphene.Int(default_value=1),
        bucket = graphene.String(default_value=BUCKET_NAME),
        prefix = graphene.String(default_value=BUCKET_PREFIX),
        is_valid = graphene.Boolean(),
        is_publish = graphene.Boolean(),
        do_diff = graphene.Boolean(default_value=False),
        description = ("Return the Mark given by mark_id, if given."

            "Otherwise, return the last previous"
            "Mark specified by the argument prev. By default prev=1 so return the previous"
            "mark. If prev=2, return the previous previous mark. If prev=3, return the"
            "previous previous previous mark, etc."

            "If is_valid or is_publish given, then filter by the condition. If both are"
            "given, then filter by both conditions being true.")
    )
    diff_marks = graphene.Field(DiffMarks,
        old_mark_id=graphene.ID(),
        new_mark_id=graphene.ID(),
        description="Return the diff and deleted parts between the two marks."
    )

    def get_condition(**args):
        if args.get('is_valid') == True:
            cond1 = FaceMarksDb.is_valid == True
        elif args.get('is_valid') == False:
            cond1 = (FaceMarksDb.is_valid == False) | (FaceMarksDb.is_valid.does_not_exist())
        else:
            cond1 = None

        if args.get('is_publish') == True:
            cond2 = FaceMarksDb.is_publish == True
        elif args.get('is_publish') == False:
            cond2 = (FaceMarksDb.is_publish == False) | (FaceMarksDb.is_publish.does_not_exist())
        else:
            cond2 = None

        if cond1!=None and cond2!=None:
            return cond1 & cond2
        elif cond1!=None and cond2==None:
            return cond1
        elif cond1==None and cond2!=None:
            return cond2
        else:
            return None

    def resolve_marks(self:FaceMarksDb, info, **args):
        condition = MarksQuery.get_condition(**args)
        return FaceMarksDb.markTypeIndex.query(FaceMarksDb.MARK_TYPE, condition, scan_index_forward=False) #type:ignore # weird msg about multiple values for scan_index_forward

    def resolve_mark(self:FaceMarksDb, info, **args) -> FaceMarksDb:
        mark_id = args.get('mark_id')
        if mark_id:
            mark = FaceMarksDb.get(mark_id)
        else:
            prev = int(args.get('prev', '1'))
            condition = MarksQuery.get_condition(**args)
            # should return the latest at index 0
            marks = list(FaceMarksDb.markTypeIndex.query(FaceMarksDb.MARK_TYPE, condition, scan_index_forward=False, limit=prev)) #type:ignore # weird msg about multiple values for scan_index_forward
            mark = marks[0] if marks else None

        return mark

    def resolve_diff_marks(self: FaceMarksDb, info, **args):
        mark, info.context.diff_parts, info.context.deleted_parts \
            = diff_marks(args.get('old_mark_id'), args.get('new_mark_id'))
        return mark


# #Relay version
# class add_Mark(graphene.ClientIDMutation):
#     mark = graphene.Field(graphene.NonNull(Marks),
#         description = '''Mark the latest file versions. e.g. addMark(input:{}) {...'''
#     )

#     class Input:
#         bucket = graphene.String(default_value=BUCKET_NAME)
#         prefix = graphene.String(default_value=BUCKET_PREFIX)

#     @classmethod
#     def mutate_and_get_payload(cls, root, info, **args):
#         mark = add_mark2(args.get('bucket'), args.get('prefix'))
#         return add_Mark(mark=mark)


class AddMarkPayload(graphene.Mutation):
    class Arguments:
        bucket = graphene.String(default_value=BUCKET_NAME)
        prefix = graphene.String(default_value='FaceAssets/Facemaker/v8/')
        dryrun = graphene.Boolean(default_value=False)

    mark = graphene.Field(graphene.NonNull(Marks),
        description = '''Mark the latest file versions. e.g. addMark {...'''
    )
    status = graphene.String(
        description = '''Indicates whether any new changes were detected or not.
            If "old" is returned, returned Mark is the previous Mark.
            If "new" is returned, returned Mark is a newly created Mark.
            '''
    )

    def mutate(self, info, **args):
        mark, status, info.context.diff_parts, info.context.deleted_parts \
            = add_mark2(args.get('bucket'), args.get('prefix'), dryrun=args.get('dryrun'))
        return AddMarkPayload(mark=mark, status=status)


class ValidateMarkPayload(graphene.Mutation):
    class Arguments:
        mark_id = graphene.ID()

    mark = graphene.Field(Marks,
        description = '''Mark the given mark_id valid'''
    )

    def mutate(self, info, **args):
        mark = validate_mark(**args)
        return ValidateMarkPayload(mark=mark)


class PublishMarkPayload(graphene.Mutation):
    class Arguments:
        mark_id = graphene.ID()

    mark = graphene.Field(Marks,
        description = '''Mark the given mark_id publish'd '''
    )

    def mutate(self, info, **args):
        mark = publish_mark(**args)
        return PublishMarkPayload(mark=mark)


class S3Version(graphene.ObjectType):
    key = graphene.String()
    version_id = graphene.String()


class PurgeUnusedVersionsMarkPayload(graphene.Mutation):
    class Arguments:
        bucket = graphene.String()
        prefix = graphene.String()
        dryrun = graphene.Boolean()

    deleted_versions = graphene.List(S3Version,
        description = '''List of deleted versions'''
    )

    def mutate(self, info, **args):
        deleted_versions = admin_purge_unused_versions(**args)
        return PurgeUnusedVersionsMarkPayload(deleted_versions=deleted_versions)


class DeleteMarkPayload(graphene.Mutation):
    class Arguments:
        mark_id = graphene.ID()

    mark = graphene.Field(Marks,
        description = '''Delete the given mark and cascade delete its parts'''
    )

    def mutate(self, info, **args):
        mark = FaceMarksDb.get(args.get('mark_id'))
        assert mark, "Given mark_id does not exist."
        mark.delete()
        return DeleteMarkPayload(mark=mark)


class MarkMutations(graphene.AbstractType):
    add_Mark = AddMarkPayload.Field(
        description='Mark the current versions of all file assets')
    validate_Mark = ValidateMarkPayload.Field()
    publish_Mark = PublishMarkPayload.Field()
    purge_unused_versions_Mark = PurgeUnusedVersionsMarkPayload.Field(
        description='Permanently delete versions from S3 which are not being used')
    delete_Mark = DeleteMarkPayload.Field()
