import os

basedir = os.path.abspath(os.path.dirname(__file__))

class BaseConfig:
    AWS_REGION='us-west-2'

    #TOKEN_EXPIRATION_DAYS = 0
    #TOKEN_EXPIRATION_SECONDS = 600
    
    # Unittesting
    UNITTEST_GROUP = 'UnitTestUsers'
    ADMINS_GROUP = 'Admins'

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(BaseConfig):
    DEBUG = True
    AWS_COGNITO_SIGNIN_URL='https://localhost:5000/auth/cognito_signin'
    AWS_COGNITO_SIGNOUT_URL='https://localhost:5000/auth/cognito_signout'
    # TestPool2
    # AWS_COGNITO_APP_WEB_DOMAIN='withme-testpool2.auth.us-west-2.amazoncognito.com'
    # AWS_COGNITO_CLIENT='6a3ufap41ig5u15bufv0lolcr4' # TestPool2
    # AWS_COGNITO_USER_POOL='us-west-2_7uh4yp56X' # TestPool2
    #
    # AWS_COGNITO_USER_POOL='us-west-2_MLxbLINkx' # testwebapp_userpool_MOBILEHUB_1585053506
    # AWS_COGNITO_CLIENT='57sa3pkot5cuankt5kc18706oc' # testwebapp_userpoolapp_MOBILEHUB_1585053506 web dev
    #
    # testwebapp_userpool_MOBILEHUB_1695894575
    AWS_COGNITO_APP_WEB_DOMAIN='withme-mega-testwebapp2.auth.us-west-2.amazoncognito.com'
    AWS_COGNITO_USER_POOL='us-west-2_vKQFhiOy2' # testwebapp_userpool_MOBILEHUB_1695894575
    AWS_COGNITO_CLIENT='4en2vu8vj9qd2989nrvorc04ao' # testwebapp_userpoolapp_MOBILEHUB_1695894575 web


class TestingConfig(BaseConfig):
    DEBUG = True
    TESTING = True
    # AWS_COGNITO_SIGNIN_URL='https://3f1yfvft63.execute-api.us-west-2.amazonaws.com/test/auth/cognito_signin'
    # AWS_COGNITO_SIGNOUT_URL='https://3f1yfvft63.execute-api.us-west-2.amazonaws.com/test/auth/cognito_signout'
    AWS_COGNITO_SIGNIN_URL='https://api.withme.live/auth/cognito_signin'
    AWS_COGNITO_SIGNOUT_URL='https://api.withme.live/auth/cognito_signout'
    # TestPool1
    # AWS_COGNITO_APP_WEB_DOMAIN='withme-testpool1.auth.us-west-2.amazoncognito.com'
    # AWS_COGNITO_CLIENT='69ger2tdv8c82kjcpnje6rqb5' # TestPool1
    # AWS_COGNITO_USER_POOL='us-west-2_l8TqJksHs' # TestPool1
    #
    # AWS_COGNITO_USER_POOL='us-west-2_MLxbLINkx' # testwebapp_userpool_MOBILEHUB_1585053506
    # AWS_COGNITO_CLIENT='1so40q18pul5kd52pmm8idr6rn' # testwebapp_userpool_MOBILEHUB_1585053506 web test
    # AWS_COGNITO_CLIENT='4en2vu8vj9qd2989nrvorc04ao' # testwebapp_userpoolapp_MOBILEHUB_1695894575 web
    #
    # testwebapp_userpool_MOBILEHUB_1695894575
    AWS_COGNITO_APP_WEB_DOMAIN='withme-mega-testwebapp2.auth.us-west-2.amazoncognito.com'
    AWS_COGNITO_USER_POOL='us-west-2_vKQFhiOy2' # testwebapp_userpool_MOBILEHUB_1695894575
    AWS_COGNITO_CLIENT='uilarijjm6fjj9dsfimlfjdud' # testwebapp_userpoolapp_MOBILEHUB_1695894575


class ProductionConfig(BaseConfig):
    DEBUG = False
    
config_map = {
    'dev': DevelopmentConfig,
    'test': TestingConfig,
    'prod': ProductionConfig,
}

def get_stage() -> str:
    return os.environ.get('STAGE','dev')

def get_project_name() -> str:
    return 'mega_project'

def get_project_path() -> str:
    return f'{get_project_name()}.{get_stage()}.'

